#include "toolbox/version.h"
#include "gem/base/version.h"
#include "gem/hardware/devices/version.h"
#include "gem/hardware/utils/version.h"
#include "gem/monitor/version.h"
#include "gem/utils/version.h"
#include "xdaq/version.h"
#include "xoap/version.h"

GETPACKAGEINFO(gem::monitor);

void gem::monitor::checkPackageDependencies()
{
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xdaq);
    CHECKDEPENDENCY(xoap);
    CHECKDEPENDENCY(gem::base);
    CHECKDEPENDENCY(gem::utils);
    CHECKDEPENDENCY(gem::hardware::devices);
    CHECKDEPENDENCY(gem::hardware::utils);
}

std::set<std::string, std::less<std::string>> gem::monitor::getPackageDependencies()
{
    std::set<std::string, std::less<std::string>> deps;
    ADDDEPENDENCY(deps, toolbox);
    ADDDEPENDENCY(deps, xoap);
    ADDDEPENDENCY(deps, xdaq);
    ADDDEPENDENCY(deps, gem::base);
    ADDDEPENDENCY(deps, gem::utils);
    ADDDEPENDENCY(deps, gem::hardware::devices);
    ADDDEPENDENCY(deps, gem::hardware::utils);
    return deps;
}
