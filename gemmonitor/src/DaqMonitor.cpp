/// class: DaqMonitor
/// description: Monitor application for GEM DAQ
///              structure borrowed from TCDS core, with nods to HCAL and EMU code
/// author: M. Dalchenko
/// date:

#include "gem/monitor/DaqMonitor.h"
#include "gem/hardware/utils/GEMCrateUtils.h"
#include <gem/rpc/amc.h>
#include <gem/rpc/amc/sca.h>
#include <gem/rpc/daq_monitor.h>

#include <bitset>
#include <gem/rpc/daq_monitor.h>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <stdexcept>

typedef gem::base::utils::GEMInfoSpaceToolBox::UpdateType GEMUpdateType;

//FIXME establish required arguments, eventually retrieve from the config
gem::monitor::DaqMonitor::DaqMonitor(const std::string& board_domain_name, log4cplus::Logger& logger, gem::base::GEMApplication* gemApp, int const& index)
    : gem::base::GEMMonitor::GEMMonitor(logger, gemApp, index)
    ,
    //xhal::client::XHALInterface(board_domain_name, logger) //FIXME: if using shared logger, then XHALInterface overtakes everything and logging from XDAQ doesn't go through
    xhal::client::XHALInterface(board_domain_name) //Works as is, providing a bit messy logging, but with all info in place
{
    CMSGEMOS_DEBUG("DaqMonitor::DaqMonitor:: entering constructor");
    if (isConnected) { //TODO Add to the app monitoring space? Need to know in order to mask in web interface the boards which failed to connect
        this->loadModule("daq_monitor", "daq_monitor v1.0.1");
        this->loadModule("amc", "amc v1.0.1");
        ohMask = xhal::client::call<::daqmon::getmonOHMask>(rpc);
        CMSGEMOS_DEBUG("DaqMonitor::DaqMonitor:: daq_monitor module loaded");
    } else {
        CMSGEMOS_INFO("DaqMonitor::DaqMonitor:: RPC interface failed to connect");
    }
    toolbox::net::URN hwCfgURN("urn:gem:hw:" + board_domain_name);
    CMSGEMOS_DEBUG("DaqMonitor::DaqMonitor:: infospace " << hwCfgURN.toString() << " does not exist, creating");
    m_is_daqmon = is_toolbox_ptr(new gem::base::utils::GEMInfoSpaceToolBox(p_gemApp,
        hwCfgURN.toString(),
        true));
    addInfoSpace("DAQ_MONITORING", m_is_daqmon, toolbox::TimeInterval(5, 0));

    buildLabelInfo();
    setupDaqMonitoring();

    logCnt = 0;

    CMSGEMOS_DEBUG("gem::monitor::DaqMonitor : constructor done");
}

gem::monitor::DaqMonitor::~DaqMonitor()
{
    CMSGEMOS_DEBUG("gem::monitor::DaqMonitor : destructor called");
    //TODO
}

void gem::monitor::DaqMonitor::setLogInfo(std::string logPath, int logInterval)
{
    m_log_path = logPath;
    m_log_interval = logInterval;
    m_write_logs = true;
}

void gem::monitor::DaqMonitor::reconnect()
{
    if (!isConnected) {
        this->connect();
        this->loadModule("daq_monitor", "daq_monitor v1.0.1");
        this->loadModule("amc", "amc v1.0.1");
        ohMask = xhal::client::call<::daqmon::getmonOHMask>(rpc);
    } else {
        CMSGEMOS_ERROR("Interface already connected. Reconnection failed");
        throw xhal::client::XHALRPCException("RPC exception: Interface already connected. Reconnection failed");
    }
}

void gem::monitor::DaqMonitor::reset()
{
    //TODO
}

void gem::monitor::DaqMonitor::addDaqMonitorable(const std::string& m_name, const std::string& m_monset)
{
    //FIXME Putting "DUMMY" as reg full name at the moment. May want to define all tables here and pass as a list to RPC
    m_is_daqmon->createUInt32(m_name, 0xFFFFFFFF, NULL, GEMUpdateType::HW32);
    addMonitorable(m_monset, "DAQ_MONITORING",
        std::make_pair(m_name, "DUMMY"),
        GEMUpdateType::HW32, "hex");
}

/// @brief Take a list of variables and replace ${} token with numbers
/// @param names Input names to be "expanded"
/// @param key Value to be replaced, of format ${XXX}
/// @param maxVal The key is replaced with numbers 0 - maxVal
static void updateByExpandingNames(std::vector<std::string>& names, const std::string& key, const int maxVal)
{

    const std::vector<std::string> expandList = names;
    names.clear();

    for (const auto& name : expandList) {
        for (int i = 0; i < maxVal; i++) {
            names.push_back(boost::replace_all_copy(name, key, std::to_string(i)));
        }
    }
}

void gem::monitor::DaqMonitor::buildLabelInfo()
{
    if (!std::getenv("CMSGEMOS_DATADIR")) {
        throw std::runtime_error("Cannot find the env variable `CMSGEMOS_DATADIR`. Make sure this is set to your install directory before running");
    }
    std::string datadir = std::getenv("CMSGEMOS_DATADIR");
    std::ifstream labelFile(datadir + "/gemmonitor/xml/labels.json");
    if (!labelFile.good()) {
        throw std::runtime_error("Cannot find monitor file: " + datadir + "/gemmonitor/xml/labels.json \n"
                                                                          "Make sure this file exists or that `CMSSGEMOS_DATADIR` is correctly set before running!");
    }

    nlohmann::json labelJson;
    try {
        labelFile >> labelJson;
    } catch (const std::exception& exc) {
        std::cerr << "[JSON ERROR]: " << exc.what();
        throw std::runtime_error(datadir + "/gemmonitor/xml/monitorables.json is not proper json! \n"
                                           "Check that the json is proper (the previous lines are the error from the json)");
    }
    for (const auto& [groupName, group] : labelJson.items()) {
        m_labelformats[groupName] = std::unordered_map<int, LabelFormat>();
        for (const auto& [hexValue, labelList] : group.items()) {
            int intValue = static_cast<int>(std::stoul(hexValue, nullptr, 16));
            if (labelList.find("value") != labelList.end()) {
                m_labelformats[groupName][intValue] = LabelFormat({ labelList["value"], labelList["class"], false });
                if (labelList.find("useHex") != labelList.end()) {
                    m_labelformats[groupName][intValue].use_hex = true;
                }
            } else {
                m_labelformats[groupName][intValue] = LabelFormat({ "", labelList["class"], true });
            }
        }
    }
}

void gem::monitor::DaqMonitor::setupDaqMonitoring()
{
    for (const auto& monname : m_monitor_groups) {
        if (monname.find("OH") != std::string::npos)
            m_oh_groups.push_back(monname);
    }
    if (!std::getenv("CMSGEMOS_DATADIR")) {
        throw std::runtime_error("Cannot find the env variable `CMSGEMOS_DATADIR`. Make sure this is set to your install directory before running");
    }
    std::string datadir = std::getenv("CMSGEMOS_DATADIR");
    std::ifstream infile(datadir + "/gemmonitor/xml/monitorables.json");
    if (!infile.good()) {
        throw std::runtime_error("Cannot find monitor file: " + datadir + "/gemmonitor/xml/monitorables.json \n"
                                                                          "Make sure this file exists or that `CMSSGEMOS_DATADIR` is correctly set before running!");
    }
    nlohmann::json jsonMonitor;
    try {
        infile >> jsonMonitor;
    } catch (const std::exception& exc) {
        std::cerr << "[JSON ERROR]: " << exc.what();
        throw std::runtime_error(datadir + "/gemmonitor/xml/monitorables.json is not proper json! \n"
                                           "Check that the json is proper (the previous lines are the error from the json)");
    }
    std::map<std::string, int> expandVars = { { "${VFAT}", 24 }, { "${GEB}", 2 }, { "${GBT}", 3 } };

    for (const auto& [groupName, group] : jsonMonitor.items()) {
        addMonitorableSet(groupName, "DAQ_MONITORING");

        std::vector<std::string> labelNameList;
        std::vector<std::string> fullNameList;

        for (const auto& [varName, varValues] : group.items()) {
            std::vector<std::string> labelNames = { varName };
            for (const auto& [expName, expVal] : expandVars) {
                if (varName.find(expName) != std::string::npos) {
                    updateByExpandingNames(labelNames, expName, expVal);
                }
            }
            if (varName.find("${OH}") != std::string::npos)
                updateByExpandingNames(labelNames, "${OH}", NOH);

            labelNameList.insert(labelNameList.end(), labelNames.begin(), labelNames.end());

            for (const auto& lblName : labelNames) {
                m_labelformat_by_name[lblName] = varValues["Label"];
                std::vector<std::string> expandedNames = { lblName };
                // logic used for creating difference in label name and variable name
                // ie used to OR groups of variables together, eg all GBT's
                // for (const auto& [expName, expVal] : expandVars) {
                //     if (varName.find(expName) != std::string::npos) {
                //         updateByExpandingNames(expandedNames, expName, expVal);
                //     }
                // }
                m_fullname_by_labelname[lblName] = expandedNames;
                fullNameList.insert(fullNameList.end(), expandedNames.begin(), expandedNames.end());
                // Add extra convertion set. May expand
                if (varValues.find("Convert") != varValues.end()) {
                    if (varValues["Convert"] == "SCA_V")
                        m_convert_by_name[lblName] = &scaVconv;
                    else if (varValues["Convert"] == "SCA_PT")
                        m_convert_by_name[lblName] = &scaPTconv;
                    else if (varValues["Convert"] == "SCA_T")
                        m_convert_by_name[lblName] = &scaTconv;
                    else if (varValues["Convert"] == "Sysmon_V")
                        m_convert_by_name[lblName] = &sysmonVconv;
                    else if (varValues["Convert"] == "Sysmon_T")
                        m_convert_by_name[lblName] = &sysmonTconv;
                }
            }
        }
        for (const auto& labelName : labelNameList) {
            m_labeldata[labelName] = new LabelData { m_board_domain_name + "." + labelName, "label label-default", "FFFFFFFF" };
        }
        m_labelname_by_group[groupName] = labelNameList;
        for (const auto& fullName : fullNameList) {
            addDaqMonitorable(fullName, groupName);
        }
    }
}

void gem::monitor::DaqMonitor::updateMonitorables()
{
    CMSGEMOS_DEBUG("DaqMonitor: Updating Monitorables");
    for (const auto& group : m_monitor_groups) {
        try {
            fillInfoSpace(group);
        } catch (...) {
            CMSGEMOS_DEBUG("DaqMonitor: Failed " + group);
            //return;
        } //FIXME Define meaningful exceptions and intercept here or eventually at a different level...
    }
    try {
        for (const auto& group : m_monitor_groups) {
            updateLabelContent(group);
        }

        // dump the monitorables every ten updates
        if (m_write_logs && logCnt % m_log_interval == 0) {
            CMSGEMOS_INFO("gem::monitor::DaqMonitor Logging monitorables");
            time_t t = time(0); // get time now
            struct tm* now = localtime(&t);
            char buffer[80];
            strftime(buffer, 80, "%Y-%m-%d-%H:%M:%S", now);
            std::string logfilename = m_log_path + "/daqmonLog_" + m_board_domain_name + "_";
            logfilename += buffer;
            logfilename += ".gz";
            std::ofstream logfile(logfilename, std::ios_base::out | std::ios_base::binary);
            bo::filtering_ostream m_out;
            m_out.push(bo::gzip_compressor());
            m_out.push(logfile);
            m_out << "Timestamp: " << buffer << std::endl;

            nlohmann::json jsonFile;
            for (const auto& [labelName, label] : m_labeldata) {
                jsonFile[label->labelId]["class_name"] = label->labelClass;
                jsonFile[label->labelId]["value"] = label->labelValue;
            }
            m_out << jsonFile.dump(4) << std::endl;
        }
        ++logCnt;

    } catch (...) {
    } //FIXME Define meaningful exceptions and intercept here or eventually at a different level...
}

void gem::monitor::DaqMonitor::fillInfoSpace(const std::string& group)
{
    CMSGEMOS_DEBUG("DaqMonitor: Update table " + group);
    std::map<std::string, uint32_t> rspMap;
    try {
        if (group == "DAQ_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonDAQmain>(rpc);
        else if (group == "DAQ_OH_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonDAQOHmain>(rpc, ohMask);
        else if (group == "DAQ_TTC_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonTTCmain>(rpc);
        else if (group == "DAQ_TRIGGER_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonTRIGGERmain>(rpc, ohMask);
        else if (group == "DAQ_TRIGGER_OH_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonTRIGGEROHmain>(rpc, ohMask);
        else if (group == "OH_MAIN")
            rspMap = xhal::client::call<::daqmon::getmonOHmain>(rpc, ohMask);
        else if (group == "OH_SYSMON")
            rspMap = xhal::client::call<::daqmon::getmonOHSysmon>(rpc, ohMask, 0);
        else if (group == "OH_GBT_LINK")
            rspMap = xhal::client::call<::daqmon::getmonGBTLink>(rpc, 0);
        else if (group == "OH_VFAT_LINK")
            rspMap = xhal::client::call<::daqmon::getmonVFATLink>(rpc, 0);
        else if (group == "OH_SCA_TEMP")
            rspMap = getSCAADCTemperature();
        else if (group == "OH_SCA_VOLTAGE")
            rspMap = getSCAADCVoltages();
        else {
            CMSGEMOS_DEBUG("DaqMonitor: Table " + group + " didn't work");
            return;
        }
    }
    STANDARD_CATCH;

    try {
        auto monlist = m_monitorableSetsMap.find(group);
        for (auto& monitem : monlist->second) {
            (monitem.second.infoSpace)->setUInt32(monitem.first, rspMap[monitem.first]);
        }
    }
    STANDARD_CATCH;
}

std::map<std::string, uint32_t> gem::monitor::DaqMonitor::getSCAADCTemperature()
{
    std::vector<std::string> names = {
        ".VTTX_CSC_PT100",
        ".VTTX_GEM_PT100",
        ".GBT0_PT100",
        ".V6_FPGA_PT100",
        ".SCA_TEMP"
    };

    std::map<std::string, uint32_t> return_map;
    auto temps = xhal::client::call<::amc::sca::readSCAADCTemperatureSensors>(rpc, ohMask);
    for (int ohidx = 0; ohidx < NOH; ohidx++) {
        std::string ohName = "OH" + std::to_string(ohidx);
        for (int i = 0; i < names.size(); ++i) {
            if (ohMask >> ohidx & 0x1) {
                auto val = (temps.at(ohidx + NOH * i)) & 0xffff;
                return_map[ohName + names.at(i)] = val;
            } else
                return_map[ohName + names.at(i)] = 0xffffffff;
        }
    }
    return return_map;
}

std::map<std::string, uint32_t> gem::monitor::DaqMonitor::getSCAADCVoltages()
{
    std::vector<std::string> names = {
        ".PROM_V1P8",
        ".VTTX_VTRX_V2P5",
        ".FPGA_CORE",
        ".SCA_V1P5",
        ".FPGA_MGT_V1P0",
        ".FPGA_MGT_V1P2"
    };

    std::map<std::string, uint32_t> return_map;
    auto voltages = xhal::client::call<::amc::sca::readSCAADCVoltageSensors>(rpc, ohMask);
    for (int ohidx = 0; ohidx < NOH; ohidx++) {
        std::string ohName = "OH" + std::to_string(ohidx);
        for (int i = 0; i < names.size(); ++i) {
            if (ohMask >> ohidx & 0x1) {
                auto val = (voltages.at(ohidx + NOH * i)) & 0xffff;
                return_map[ohName + names.at(i)] = val;
            } else
                return_map[ohName + names.at(i)] = 0xffffffff;
        }
    }
    return return_map;
}

double gem::monitor::DaqMonitor::scaVconv(uint32_t val)
{
    return static_cast<double>(val * (0.244 / 1000) * 3.0);
}

double gem::monitor::DaqMonitor::scaTconv(uint32_t val)
{
    return static_cast<double>(val * (-0.53) + 381.2);
}

double gem::monitor::DaqMonitor::scaPTconv(uint32_t val)
{
    return static_cast<double>(0.2597 * (9891.8 - 2.44 * val));
}

double gem::monitor::DaqMonitor::sysmonTconv(uint32_t val)
{
    return static_cast<double>(val * 0.49 - 273.15);
}

double gem::monitor::DaqMonitor::sysmonVconv(uint32_t val)
{
    return static_cast<double>(val * 2.93 / 1000);
}

void gem::monitor::DaqMonitor::updateLabelContent(const std::string& group)
{
    CMSGEMOS_DEBUG("DaqMonitor::updateLabelContent " + group);
    uint32_t val, tmpval;
    LabelData* ld;
    for (const auto& monname : m_labelname_by_group[group]) {
        val = m_is_daqmon->getUInt32(m_fullname_by_labelname[monname].front());
        if (m_fullname_by_labelname[monname].size() > 1) {
            val = 0xFFFFFFFF;
            for (auto fullname : m_fullname_by_labelname[monname]) {
                // To be filled with AND logic.
                // Idea being, combine VFAT or GBT info to one value per OH
                tmpval = m_is_daqmon->getUInt32(fullname);
                if (tmpval == 0xFFFFFFFF || tmpval == 0xDEADDEAD)
                    continue;
                if (val == 0xFFFFFFFF || val == 0xDEADDEAD) {
                    val = tmpval;
                } else if (m_labelformat_by_name[monname] == "ShouldBeOne") {
                    val += tmpval;
                } else if (m_labelformat_by_name[monname] == "ErrorIfNotZero") {
                    val &= tmpval;
                }
            }
        }

        ld = m_labeldata.find(monname)->second;
        auto labelDet = m_labelformats[m_labelformat_by_name[monname]];
        if (labelDet.find(val) != labelDet.end()) {
            ld->labelValue = labelDet[val].lb_value;
            ld->labelClass = labelDet[val].lb_class;
        } else {
            ld->labelClass = labelDet[-2].lb_class;
            if (labelDet[-2].use_val && labelDet[-2].use_hex) {
                std::stringstream ss;
                ss << std::uppercase << std::setfill('0')
                   << std::setw(8) << std::hex << val << std::dec;
                ld->labelValue = ss.str();
            } else if (!labelDet[-2].use_val) {
                ld->labelValue = labelDet[-2].lb_value;
            } else if (m_convert_by_name[monname] != nullptr) {
                ld->labelValue = std::to_string((m_convert_by_name[monname])(val));
            } else {
                ld->labelValue = std::to_string(val);
            }
        }
    }
}

void gem::monitor::DaqMonitor::buildTable(const std::string& table_name, xgi::Output* out)
{
    CMSGEMOS_DEBUG("DaqMonitor: Build DAQ main table");
    std::vector<std::string> labelNames;

    if (table_name == "DAQ_MAIN") {
        updateLabelContent("DAQ_MAIN");
        labelNames = m_labelname_by_group["DAQ_MAIN"];
    } else if (table_name == "DAQ_TTC_MAIN") {
        updateLabelContent("DAQ_TTC_MAIN");
        labelNames = m_labelname_by_group["DAQ_TTC_MAIN"];
    } else if (table_name == "DAQ_TRIGGER_MAIN") {
        updateLabelContent("DAQ_TRIGGER_MAIN");
        labelNames = m_labelname_by_group["DAQ_TRIGGER_MAIN"];

    } else if (table_name == "OH_MAIN") {
        for (const auto& group : m_oh_groups) {
            updateLabelContent(group);
            auto tmpVec = m_labelname_by_group[group];
            labelNames.insert(labelNames.end(), tmpVec.begin(), tmpVec.end());
        }
    }

    *out << "<font size=\"5\">" << std::endl;
    *out << "<small>" << std::endl;
    *out << "<table align=\"center\" class=\"table table-bordered table-condensed\" style=\"width:100%\">" << std::endl;
    LabelData* ld;
    if (table_name == "OH_MAIN") {
        *out << "    <tr>" << std::endl;
        *out << "    <td style=\"width:10%\">"
             << "REGISTER|OH"
             << "</td>" << std::endl;
        for (int i = 0; i < NOH; ++i) {
            *out << "<td>" << std::to_string(i) << "</td>";
        }
        *out << "    </tr>" << std::endl;
    }

    if (table_name == "OH_MAIN") {
        for (int i = 0; i < labelNames.size(); i += NOH) {
            std::string shortName = labelNames.at(i).substr(4);
            *out << "    <tr>" << std::endl;
            *out << "    <td style=\"width:10%\">" << shortName << "</td>" << std::endl;
            for (int j = 0; j < NOH; j++) {
                ld = m_labeldata.find(labelNames.at(i + j))->second;
                *out << "<td><span id=\"" << ld->labelId << "\" class=\"" << ld->labelClass << "\">" << ld->labelValue << "</span></td>" << std::endl;
            }
            *out << "    </tr>" << std::endl;
        }
    } else {
        for (const auto& monname : labelNames) {
            ld = m_labeldata.find(monname)->second;
            *out << "    <tr>" << std::endl;
            *out << "    <td style=\"width:10%\">" << monname << "</td>" << std::endl;
            *out << "<td><span id=\"" << ld->labelId << "\" class=\"" << ld->labelClass << "\">" << ld->labelValue << "</span></td>" << std::endl;
            *out << "    </tr>" << std::endl;
        }
    }
    *out << "</table>" << std::endl;
    *out << "</small>" << std::endl;
    *out << "</font>" << std::endl;
}

void gem::monitor::DaqMonitor::buildMonitorPage(xgi::Output* out)
{
    *out << "<div class=\"col-lg-3\">" << std::endl
         << "  <div class=\"panel panel-default\">" << std::endl
         << "    <div class=\"panel-heading\">" << std::endl
         << "      <h4 align=\"center\">" << std::endl
         << "        DAQ" << std::endl
         << "      </h4>" << std::endl;
    //FIXME add IEMASK later
    buildTable("DAQ_MAIN", out);
    *out << "    </div>" << std::endl // end panel head
         // There could be a panel body here
         << "  </div>" << std::endl // end panel
         // There could be other elements in the column...
         << "  <div class=\"panel panel-default\">" << std::endl
         << "    <div class=\"panel-heading\">" << std::endl
         << "      <h4 align=\"center\">" << std::endl
         << "        TTC" << std::endl
         << "      </h4>" << std::endl;
    buildTable("DAQ_TTC_MAIN", out);
    *out << "    </div>" << std::endl // end panel head
         // There could be a panel body here
         << "  </div>" << std::endl // end panel
         << "  <div class=\"panel panel-default\">" << std::endl
         << "    <div class=\"panel-heading\">" << std::endl
         << "      <h4 align=\"center\">" << std::endl
         << "        Trigger Rate" << std::endl
         << "      </h4>" << std::endl;
    buildTable("DAQ_TRIGGER_MAIN", out);
    *out << "    </div>" << std::endl // end panel head
         // There could be a panel body here
         << "  </div>" << std::endl // end panel

         << "</div>" << std::endl // end column
         << "<div class=\"col-lg-9\">" << std::endl
         << "  <div class=\"panel panel-default\">" << std::endl
         << "    <div class=\"panel-heading\">" << std::endl
         << "      <h4 align=\"center\">" << std::endl
         << "        OPTICAL LINKS" << std::endl
         << "      </h4>" << std::endl;
    //FIXME add IEMASK later
    buildTable("OH_MAIN", out);
    *out << "    </div>" << std::endl // end panel head
         // There could be a panel body here
         << "  </div>" << std::endl // end panel
         // There could be other elements in the column...
         << "</div>" << std::endl; // end column
}

void gem::monitor::DaqMonitor::jsonContentUpdate(xgi::Output* out)
{
    CMSGEMOS_DEBUG("DaqMonitor::jsonContentUpdate");
    nlohmann::json jsonFile;
    for (auto group : m_monitor_groups) {
        for (auto labelName : m_labelname_by_group[group]) {
            auto label = m_labeldata[labelName];
            jsonFile[label->labelId]["class_name"] = label->labelClass;
            jsonFile[label->labelId]["value"] = label->labelValue;
        }
    }
    *out << jsonFile.dump(4) << std::endl;
}
