/// @file gem/daqmon/exception/Exception.h

#ifndef GEM_MONITOR_EXCEPTION_EXCEPTION_H
#define GEM_MONITOR_EXCEPTION_EXCEPTION_H

#include <string>

#include "gem/utils/exception/Exception.h"

// The gem::daqmon exceptions.
#define GEM_MONITOR_DEFINE_EXCEPTION(EXCEPTION_NAME) GEM_DEFINE_EXCEPTION(EXCEPTION_NAME, monitor)
GEM_MONITOR_DEFINE_EXCEPTION(Exception)
GEM_MONITOR_DEFINE_EXCEPTION(SoftwareProblem)
GEM_MONITOR_DEFINE_EXCEPTION(ValueError)

// The gem::daqmon alarms.
#define GEM_MONITOR_DEFINE_ALARM(ALARM_NAME) GEM_MONITOR_DEFINE_EXCEPTION(ALARM_NAME)
GEM_MONITOR_DEFINE_ALARM(MonitoringFailureAlarm)

#endif // GEM_MONITOR_EXCEPTION_EXCEPTION_H
