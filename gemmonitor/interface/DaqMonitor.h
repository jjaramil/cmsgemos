/// @file DAQMonitor.h

#ifndef GEM_MONITOR_DAQMONITOR_H
#define GEM_MONITOR_DAQMONITOR_H

#include "gem/base/GEMApplication.h"
#include "gem/base/GEMMonitor.h"
//#include "xhal/rpc/utils.h"
#include <xhal/client/XHALInterface.h>
#include <xhal/client/call.h>

#include <fstream>
#include <iostream>
#include <time.h>

#include <boost/algorithm/string/replace.hpp>
#include <boost/iostreams/copy.hpp>
#include <boost/iostreams/filter/gzip.hpp>
#include <boost/iostreams/filtering_stream.hpp>
#include <boost/iostreams/filtering_streambuf.hpp>

namespace bo = boost::iostreams;

namespace gem {
namespace base {
    class GEMApplication;
}
namespace monitor {

    class DaqMonitor : public gem::base::GEMMonitor, public xhal::client::XHALInterface {
    public:
        /// Holder for general format for the monitorables in the webpage
        ///
        /// This data is derived from the /gemmonitor/xml/labels.json file
        /// The intent is to allow different variables to have differently defined
        /// looks in the webpage depending on expect value (eg for different default
        /// values make the text green).
        struct LabelFormat {
            std::string lb_value;
            std::string lb_class;
            bool use_val;
            bool use_hex;
        };

        /// Holder for html quantities that a monitorable variable will gain in the webpage
        ///
        /// Class that holds actual html quantities to be used in the next webpage update
        struct LabelData {
            std::string labelId;
            std::string labelClass;
            std::string labelValue;
        };

        /// @brief Constructor from GEMMonitor derived class
        /// @param board_domain_name The domain name of the AMC card
        /// @param optohybridManager logger Log4cplus logger
        /// @param gemApp Calling GEMApplication instance
        /// @param index Index
        DaqMonitor(const std::string& board_domain_name, log4cplus::Logger& logger, base::GEMApplication* gemApp, int const& index);

        virtual ~DaqMonitor();

        virtual void reconnect();

        virtual void updateMonitorables();

        /// @brief Calls RPC function to for input group (e.g. DAQ, OH, TTC) then fills InfoSpace with respective values
        /// @param group Name of group to be parsed
        void fillInfoSpace(const std::string& group);

        //all voltages are divided by 3 on the OH board by a voltage divider (temperature sensor voltage is not divided).
        //All values are in units of ADC counts, and this is a 12bit ADC with a range of 0.0 - 1.0V, so each count is 1V / 4095 = 0.244mV.
        static double scaVconv(uint32_t val);
        //There's not much information about the SCA chip temperature units, but some information can be found in the SCA manual page 52, which shows linear dependence of temperature from -37.5 deg C to 79 deg C corresponding to 790 counts for -37.5degC and going down to about 570 counts for 79degC.
        static double scaTconv(uint32_t val);
        //For PT100 measurements, a more complex conversion is necessary to covert the ADC counts to temperature. The way it works is that the ADC supplies 100uA current that goes through the PT100 sensor to ground, and the voltage drop over the sensor then corresponds to the temperature. The exact part number of the PT100 sensor we are using is P0K1.1206.2P.B, which has a resistance of 100 Ohms at 0 deg C, and changes by 0.385% per each degree C cumulatively (more info can be found here: http://www.farnell.com/datasheets/2207165.pdf?_ga=2.247382788.1064362929.1535377707-998824428.1535377707 and here: https://www.intech.co.nz/products/temperature/typert/RTD-Pt100-Conversion.pdf). This means that at 0 deg C the voltage drop over the PT100 will be 100uA * 100 Ohms = 10mV, so the ADC will read 1.0V - 10mV = 0.99V, and if the temperature is 30 degC, the PT100 will have a resistance of 111.67 Ohms, and so the voltage drop will be 100uA * 111.67 Ohms = 11.167mV, so the ADC will read 1.0V - 11.167mV = 0.988833V.
        static double scaPTconv(uint32_t val);
        static double sysmonTconv(uint32_t val);
        static double sysmonVconv(uint32_t val);

        virtual void reset();

        /// @brief helper function to constructor. Setup Infospace variables and private map
        void setupDaqMonitoring();

        /// @brief Wraper for adding monitorable and variable to InfoSpace
        /// @param m_name Full name of variable
        /// @param m_monset Set the variable is apart of
        void addDaqMonitorable(const std::string& m_name, const std::string& m_monset);

        /// @brief display the monitor items
        void buildMonitorPage(xgi::Output* out);

        void buildTable(const std::string& table_name, xgi::Output* out);

        /// @brief Read in labels.json file and built m_labelformats variable
        void buildLabelInfo();

        typedef std::shared_ptr<gem::base::utils::GEMInfoSpaceToolBox> is_toolbox_ptr;

        /// @brief Put rpc information into the appropriate LabelData object
        /// @param group Group that is parsed over from monitro_groups
        void updateLabelContent(const std::string& group);

        /// @brief Put values/information in m_labeldata into json format
        void jsonContentUpdate(xgi::Output* out);

        bool is_connected() { return isConnected; }

        std::string boardName() { return m_board_domain_name; }

        /// @brief Turn on logfile creation and set variables associated with that
        /// @param logPath directory where log files are saved
        /// @param logInterval How many update cycles between log write
        void setLogInfo(std::string logPath, int logInterval);

    protected:
        void init();
        inline constexpr static int NOH = 12;
        int ohMask = 0xfff;

    private:
        std::map<std::string, uint32_t> getSCAADCTemperature();
        std::map<std::string, uint32_t> getSCAADCVoltages();

        is_toolbox_ptr m_is_daqmon;

        std::unordered_map<std::string, LabelData*> m_labeldata;
        // Maps variables -> map of special values (eg 0xFFFFFFFF) to the LabelFormat of that special value.
        // -2 is used for any other value (because -1 -> 0xFFFFFFFF in int)
        std::unordered_map<std::string, std::unordered_map<int, LabelFormat>> m_labelformats;

        // NOTE: labelname -> name used in webpage, ie ${OH} replaced, but not ${VFAT} etc
        std::unordered_map<std::string, std::string> m_labelformat_by_name;
        std::map<std::string, std::vector<std::string>> m_fullname_by_labelname;
        std::map<std::string, std::vector<std::string>> m_labelname_by_group;

        std::vector<std::string> m_monitor_groups = {
            // list of groups in monitorables.json actually monitored
            "DAQ_MAIN",
            "DAQ_TTC_MAIN",
            "DAQ_TRIGGER_MAIN",
            "DAQ_TRIGGER_OH_MAIN", // link all 0xffff ?
            "DAQ_OH_MAIN",
            "OH_MAIN", //fw_version
            "OH_GBT_LINK",
            "OH_VFAT_LINK",
            "OH_SCA_TEMP",
            "OH_SCA_VOLTAGE",
            "OH_SYSMON",
        };
        std::vector<std::string> m_oh_groups; /// Groups that replace ${OH} (has OH in name)
        std::unordered_map<std::string, double (*)(uint32_t)> m_convert_by_name;
        int logCnt;

        bool m_write_logs = false;
        std::string m_log_path;
        int m_log_interval;

    }; // class DaqMonitor

} // namespace gem::monitor
} // namespace gem

#endif // GEM_MONITOR_MONITOR_H
