# Developing cmsgemos

* [Development Setup](#setup)
* [Running Tests](#tests)
* [Code Style Guidelines](#rules)
* [Writing Documentation](#documentation)

## <a name="setup"> Development Setup
### Development requirements

A successful complete build requires the following tools to be installed and activated:

* `devtoolset-8`
* `arm-linux-gnueabihf-g++` version 4.9 from the Xilinx SDK or project Linaro
* `cmake3` (available in EPEL)
* `ninja` (optional; provides faster builds)

In addition, the `llvm-toolset-7.0` can be installed to get access to the `clang-*` tools.
Do **not** install `llvm-toolset-7.0-cmake` which contains too old `cmake` release.

Developers using the `gem904daq04` machine, can source this script before starting their developments:

``` bash
# Newer SW releases
source scl_source enable rh-git218
source scl_source enable llvm-toolset-7.0
source scl_source enable devtoolset-8

# Xilinx ARM compiler
source /data/bigdisk/sw/Xilinx/SDK/2016.2/settings64.sh
```

### Build instructions

* Clone the repository `git clone https://gitlab.cern.ch/cmsgemonline/cmsgemos.git && cd cmsgemos`

To build the DAQ machine library and applications, use the following instructions:

* Configure the build with `cmake3 -G "Ninja" -B _build .`
  (To do only once, or when configuration parameters change.)
* Build and install locally the software with `cmake3 --build _build --target install`
  The local installation is available under `_build/_install`
* You can trigger the tests with `cmake3 --build _build --target test`

To build the RPC modules, use the following instructions:

* Go to the `gemrpc` sub-directory `cd gemrpc`
* Configure the build with `cmake3 -G "Ninja" -B _build -DCMAKE_TOOLCHAIN_FILE=../cmake/ToolchainCTP7.cmake -DPETA_SYSROOT=/opt/gem-peta-stage/ctp7/ .`
  (To do only once, or when configuration parameters change.)
* Build and install in a local tree back-end software `DESTDIR=_install cmake3 --build _build --target install`
  The local installation is available under `_build/_install`

The software is deployed on the machines as RPM packages.
Maintainers can build test packages locally with the `tito` program:

```
tito build --offline --rpm --test
```

Developers are encouraged to test the RPM package build when introducing changes in the build system or the SPEC file.

## <a name="tests"> Running Tests
For the moment autotests are not supported (but will be added in the near future). Thus you should manually 
test your code before opening a merge request. In case you need a hardware setup,
reserve it in [SuperSaaS][supersaas]. If you don't have the shared user password, 
refer to the GEM DAQ Software team managers.
After you're done with your tests, return the system to the initial state. 

Example configuration and instructions on manual running are described [here](doc/examples/README.md).

## <a name="rules"> Code Style Guidelines
We are following WebKit [Code Style Guideline][webkit] for `C++` and [PEP8][pep8] standard for python code.
We also find very useful [C++ Core Guidelines][cppcore] by Bjarne Stroustrup and Herb Sutter and strongly encourage you to read them too.

## <a name="documentation"> Writing Documentation
We use the [Doxygen][doxygen] for `C++` API documentation and [sphinx][sphinx] for `python` code documentation.
The documentation can be generated using the following command:

```bash
cmake3 --build _build --target doc
```

Generated files are placed in `_build/html`.

This section will be completed later with precise documentation style guide, for the moment follow the examples from the code itself.

[supersaas]:https://www.supersaas.com/schedule/supervise/GEM_904_Infrastructure
[webkit]:https://webkit.org/code-style-guidelines/
[cppcore]:https://github.com/isocpp/CppCoreGuidelines/blob/master/CppCoreGuidelines.md
[pep8]:https://www.python.org/dev/peps/pep-0008/
[doxygen]:http://www.doxygen.nl/manual/starting.html
[sphinx]:https://www.sphinx-doc.org/en/master/usage/quickstart.html
