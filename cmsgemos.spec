Name:    cmsgemos
Epoch:   1
Version: 0.0
Release: 2
Summary: GEM Online Software
License: Unknown
URL:     https://gitlab.cern.ch/cmsgemonline/%{name}
Source0: %{name}-%{version}.tar.gz

# Build tools
BuildRequires: cmake3 >= 3.11
BuildRequires: devtoolset-8-binutils
BuildRequires: devtoolset-8-gcc-c++
BuildRequires: ninja-build >= 1.7

# System libraries
BuildRequires: boost-devel >= 1.53
BuildRequires: libuuid-devel
BuildRequires: protobuf-devel
BuildRequires: protobuf-lite-devel
BuildRequires: yaml-cpp-devel
BuildRequires: zlib-devel

# CACTUS
BuildRequires: cactusboards-amc13-amc13
BuildRequires: cactuscore-uhal-log
BuildRequires: cactuscore-uhal-uhal

# xDAQ (aka CMSOS)
# -devel packages do not depend on the corresponding binary packages
BuildRequires: cmsos-core-asyncresolv, cmsos-core-asyncresolv-devel
BuildRequires: cmsos-core-cgicc, cmsos-core-cgicc-devel
BuildRequires: cmsos-core-config, cmsos-core-config-devel
BuildRequires: cmsos-core-log4cplus, cmsos-core-log4cplus-devel
BuildRequires: cmsos-core-logudpappender, cmsos-core-logudpappender-devel
BuildRequires: cmsos-core-logxmlappender, cmsos-core-logxmlappender-devel
BuildRequires: cmsos-core-mimetic, cmsos-core-mimetic-devel
BuildRequires: cmsos-core-nlohmannjson, cmsos-core-nlohmannjson-devel
BuildRequires: cmsos-core-pt, cmsos-core-pt-devel
BuildRequires: cmsos-core-toolbox, cmsos-core-toolbox-devel
BuildRequires: cmsos-core-xalan, cmsos-core-xalan-devel
BuildRequires: cmsos-core-xcept, cmsos-core-xcept-devel
BuildRequires: cmsos-core-xdaq, cmsos-core-xdaq-devel
BuildRequires: cmsos-core-xdata, cmsos-core-xdata-devel
BuildRequires: cmsos-core-xerces, cmsos-core-xerces-devel
BuildRequires: cmsos-core-xgi, cmsos-core-xgi-devel
BuildRequires: cmsos-core-xoap, cmsos-core-xoap-devel
BuildRequires: cmsos-worksuite-oracle, cmsos-worksuite-oracle-devel
BuildRequires: cmsos-worksuite-tstore, cmsos-worksuite-tstore-devel
BuildRequires: cmsos-worksuite-tstoreclient, cmsos-worksuite-tstoreclient-devel
BuildRequires: cmsos-worksuite-tstoreutils, cmsos-worksuite-tstoreutils-devel
BuildRequires: cmsos-worksuite-xdaq2rc, cmsos-worksuite-xdaq2rc-devel

%description
Online Software for the CMS GEM detectors.

%prep
%setup -q

%build
source /opt/rh/devtoolset-8/enable
%cmake3 -DCMAKE_BUILD_TYPE=RelWithDebInfo -S . -B _build -G Ninja
%__cmake3 --build _build

%install
source /opt/rh/devtoolset-8/enable
DESTDIR=%{buildroot} %__cmake3 --build _build --target install

%check
# Run unit tests
source /opt/rh/devtoolset-8/enable
%__cmake3 --build _build --target test

%files
%doc CONTRIBUTORS LICENSE doc/examples
%{_libdir}/libgem*.so
%{_datadir}/%{name}/

%post
# Create the "gemdaq" symbolic link in xDAQ's htdocs directory
if [ $1 -ge 1 ] && [ -d /opt/xdaq/htdocs ]; then
    ln -snf %{_datadir}/%{name}/htdocs/gemdaq /opt/xdaq/htdocs
fi

%preun
# Remove the "gemdaq" symbolic link
if [ $1 == 0 ] && [ -h /opt/xdaq/htdocs/gemdaq ]; then
    rm /opt/xdaq/htdocs/gemdaq
fi

%changelog
* Wed Sep 09 2020 Laurent Pétré <laurent.petre@cern.ch> 0.0-2
- Build package with tito

* Wed Sep 09 2020 Laurent Pétré <laurent.petre@cern.ch> - 0.0-1
- Update the package build dependencies
- Set the package version to the latest tagged release
- Bump the Epoch value

* Fri Aug 28 2020 Louis Moureaux <louis.moureaux@cern.ch> - 1.0-3
- Add documentation

* Fri Aug 28 2020 Louis Moureaux <louis.moureaux@cern.ch> - 1.0-2
- Add the gemdaq symlink in xDAQ's htdocs directory

* Tue Aug 25 2020 Louis Moureaux <louis.moureaux@cern.ch> - 1.0-1
- Initial packaging
