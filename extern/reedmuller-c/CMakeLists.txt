cmake_minimum_required(VERSION 3.11 FATAL_ERROR)

project(reedmuller-c)

add_library(reedmuller-c STATIC
    combination.cpp
    ksubset.cpp
    matrix.cpp
    reedmuller.cpp
    vector.cpp
)
set_property(TARGET reedmuller-c PROPERTY POSITION_INDEPENDENT_CODE ON)

target_include_directories(reedmuller-c PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}")
