/// @file AMCManager.cpp

#include "gem/hardware/managers/AMCManager.h"

#include "gem/hardware/devices/amc/HwGenericAMC.h"
#include "gem/hardware/devices/optohybrid/HwOptoHybrid.h"
#include "gem/hardware/managers/AMCManagerWeb.h"
#include "gem/hardware/managers/exception/Exception.h"
#include "gem/hardware/managers/optohybrid/exception/Exception.h"
#include "gem/hardware/utils/GEMCrateUtils.h"

#include "xoap/AttachmentPart.h"
#include "xoap/MessageFactory.h"
#include "xoap/MessageReference.h"
#include "xoap/Method.h"
#include "xoap/SOAPBody.h"
#include "xoap/SOAPConstants.h"
#include "xoap/SOAPEnvelope.h"
#include "xoap/domutils.h"

#include <iterator>

XDAQ_INSTANTIATOR_IMPL(gem::hardware::AMCManager);

gem::hardware::AMCManager::AMCManager(xdaq::ApplicationStub* stub)
    : gem::base::GEMFSMApplication(stub)
{
    p_gemWebInterface = new gem::hardware::AMCManagerWeb(this);

    p_appInfoSpace->fireItemAvailable("crate", &m_crate);
    p_appInfoSpace->fireItemAvailable("slot", &m_slot);
    p_appInfoSpace->fireItemAvailable("optohybridMask", &m_optohybridMask);
    p_appInfoSpace->fireItemAvailable("enableZS", &m_enableZS);
    p_appInfoSpace->fireItemAvailable("autoKillOptoHybrid", &m_autoKillOptoHybrid);

    p_appInfoSpace->addItemRetrieveListener("crate", this);
    p_appInfoSpace->addItemRetrieveListener("slot", this);
    p_appInfoSpace->addItemRetrieveListener("optohybridMask", this);
    p_appInfoSpace->addItemRetrieveListener("enableZS", this);
    p_appInfoSpace->addItemRetrieveListener("autoKillOptoHybrid", this);

    p_appInfoSpace->addItemChangedListener("crate", this);
    p_appInfoSpace->addItemChangedListener("slot", this);
    p_appInfoSpace->addItemChangedListener("optohybridMask", this);
    p_appInfoSpace->addItemChangedListener("enableZS", this);
    p_appInfoSpace->addItemChangedListener("autoKillOptoHybrid", this);

    xoap::bind<gem::base::GEMApplication>(this, &gem::base::GEMApplication::calibParamRetrieve, "calibParamRetrieve", XDAQ_NS_URI);
    xoap::bind(this, &gem::hardware::AMCManager::setRunTypeInterCalib, "setRunTypeInterCalib", XDAQ_NS_URI);
    xoap::bind(this, &gem::hardware::AMCManager::updateRunParamCalib, "updateRunParamCalib", XDAQ_NS_URI);
    xoap::bind(this, &gem::hardware::AMCManager::updateScanValueCalib, "updateScanValueCalib", XDAQ_NS_URI);

    init();
}

gem::hardware::AMCManager::~AMCManager()
{
}

// This is the callback used for handling xdata:Event objects
void gem::hardware::AMCManager::actionPerformed(xdata::Event& event)
{
    if (event.type() == "setDefaultValues" || event.type() == "urn:xdaq-event:setDefaultValues") {
        CMSGEMOS_DEBUG("AMCManager::actionPerformed() setDefaultValues");
    }

    gem::base::GEMApplication::actionPerformed(event);
}

void gem::hardware::AMCManager::init()
{
}

// State transitions
void gem::hardware::AMCManager::initializeAction()
{
    CMSGEMOS_INFO("AMCManager::initializeAction begin");

    // == AMC ==
    const std::string deviceName = toolbox::toString("gem-shelf%02d-amc%02d", m_crate.value_, m_slot.value_);
    try {
        m_amc = std::make_shared<gem::hardware::amc::HwGenericAMC>(deviceName);
    }
    GEM_HARDWARE_TRANSITION_CATCH("AMCManager::initializeAction", gem::hardware::devices::exception::Exception);

    CMSGEMOS_INFO("AMCManager::initializeAction: recover AMC");
    try {
        m_amc->recoverAMC();
    }
    GEM_CATCH_RPC_ERROR("AMCManager::initializeAction", gem::hardware::devices::exception::ConfigurationProblem);

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        const std::string deviceName = toolbox::toString("gem-shelf%02d-amc%02d-optohybrid%02d", m_crate.value_, m_slot.value_, link);
        try {
            m_optohybrids.at(link) = std::make_shared<gem::hardware::optohybrid::HwOptoHybrid>(deviceName);
        }
        GEM_HARDWARE_TRANSITION_CATCH("AMCManager::initializeAction", gem::hardware::devices::exception::Exception);
    }

    CMSGEMOS_INFO("AMCManager::initializeAction end");
}

void gem::hardware::AMCManager::configureAction()
{
    CMSGEMOS_INFO("AMCManager::configureAction begin");

    // == AMC ==
    if (!m_amc->isHwConnected()) {
        std::stringstream errmsg;
        errmsg << "AMCManager::configureAction AMC in slot " << m_slot.value_ << " is not connected";
        CMSGEMOS_ERROR(errmsg.str());
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, errmsg.str());
    }

    CMSGEMOS_INFO("AMCManager::configureAction: reset clocks");
    try {
        m_amc->resetClocks();
    }
    GEM_CATCH_RPC_ERROR("AMCManager::configureAction", gem::hardware::devices::exception::ConfigurationProblem);

    CMSGEMOS_INFO("AMCManager::configureAction: establish front-end communication");
    try {
        const uint32_t workingOptoHybridMask = m_amc->establishFrontEndCommunication(m_optohybridMask.value_);

        if (m_autoKillOptoHybrid.value_) {
            m_optohybridMask.value_ = workingOptoHybridMask;
        } else if (m_optohybridMask != workingOptoHybridMask) {
            std::stringstream errmsg;
            errmsg << "AMCManager::configureAction: impossible to establish communication with all OptoHybrids";
            CMSGEMOS_ERROR(errmsg.str());
            XCEPT_RAISE(gem::hardware::devices::exception::Exception, errmsg.str());
        }

        CMSGEMOS_INFO("AMCManager::configureAction: using optohybridMask: 0x" << std::hex << m_optohybridMask);
    }
    GEM_CATCH_RPC_ERROR("AMCManager::configureAction", gem::hardware::devices::exception::ConfigurationProblem);

    CMSGEMOS_INFO("AMCManager::configureAction: configure DAQ module");
    try {
        m_amc->configureDAQModule(m_optohybridMask.value_, m_enableZS.value_);
    }
    GEM_CATCH_RPC_ERROR("AMCManager::configureAction", gem::hardware::devices::exception::ConfigurationProblem);

    // Calibration
    if (m_scanInfo.bag.scanType.value_ == 2) {
        m_amc->setDAQLinkRunType(0x2);
        uint32_t runParams = ((0x1 << 22) | (0x0 << 21) | (0x6 << 13) | ((m_scanInfo.bag.mspl.value_ & 0x7) << 10) | (m_scanInfo.bag.scanMin.value_ & 0x3ff));
        //                       isExtTrig; isCurrentPulse; CFG_CAL_DAC;   MSPL/\;

        CMSGEMOS_INFO("AMCManager::configureAction: AMC runParams: " << runParams); //CG: TO be removed
        m_amc->setDAQLinkRunParameters(runParams);

        // If calibration format mode invoked for data
        // setting caldata format here; and in OHmanager set the channels for the calmode
        CMSGEMOS_INFO("AMCManager::configureAction: m_scanInfo.bag.calMode.value_: " << m_scanInfo.bag.calMode.value_); ///CG remove
        m_amc->configureAMCCalDataFormat(m_scanInfo.bag.calMode.value_);

        // taken from Brian's macro
        m_amc->writeReg("GEM_AMC.TTC.GENERATOR.ENABLE", 0x0);

        m_amc->writeReg("GEM_AMC.TTC.CTRL.CALPULSE_L1A_DELAY", 0x0);
        m_amc->writeReg("GEM_AMC.TTC.CTRL.CALIBRATION_MODE", 0x0);

        if (m_scanInfo.bag.signalSourceType.value_ == 0) { // use calibration pulse from the TTC internally generated in the AMC
            m_amc->writeReg("GEM_AMC.TTC.CTRL.CALPULSE_L1A_DELAY", 50); // send the L1A N BX after the calibration pulse
            m_amc->writeReg("GEM_AMC.TTC.CTRL.CALIBRATION_MODE", 0x1);
        }
    } else if (m_scanInfo.bag.scanType.value_ == 3) {
        uint32_t initialVT1 = m_scanInfo.bag.scanMin.value_;
        uint32_t initialVT2 = 0; // std::max(0,(uint32_t)m_scanInfo.bag.scanMax.value_);
        CMSGEMOS_INFO("AMCManager::configureAction: FIRST VT1 " << initialVT1 << " VT2 " << initialVT2);

        m_amc->setDAQLinkRunType(0x3);
        // m_amc->setDAQLinkRunParameter(0x1,latency);  // set this at start so DQM has it?
        m_amc->setDAQLinkRunParameter(0x2, initialVT1);
        m_amc->setDAQLinkRunParameter(0x3, initialVT2);
    }

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);

        if (!optohybrid->isHwConnected()) {
            std::stringstream errmsg;
            errmsg << "AMCManager::configureAction: OptoHybrid connected on link " << link << " to AMC in slot " << m_slot.value_ << " is not responding";
            CMSGEMOS_ERROR(errmsg.str());
            XCEPT_RAISE(gem::hardware::managers::optohybrid::exception::Exception, errmsg.str());
        }

        // First of all, update the connected VFAT list
        // Since front-end access is forbiden in the initialization step,
        // this is the only place where it can be done while waiting for the DB
        m_vfatMask.at(link) = ~(optohybrid->getConnectedVFATMask(true));
        const uint32_t vfatMask = m_vfatMask.at(link);

        CMSGEMOS_INFO("AMCManager::configureAction: configure VFATs");
        optohybrid->configureVFATs();

        // Calibration
        CMSGEMOS_INFO("AMCManager::configureAction: beginning specifics for latency");

        if (m_scanInfo.bag.scanType.value_ == 2) {
            //TODO:set calibration mode (atm set to false) data format
            if (m_scanInfo.bag.calMode.value_)
                optohybrid->configureOHCalDataFormat(vfatMask);

            CMSGEMOS_DEBUG("AMCManager::configureAction: Latency scan with value " << m_scanInfo.bag.scanMin.value_);
            optohybrid->broadcastWrite("CFG_LATENCY", m_scanInfo.bag.scanMin.value_, vfatMask, false);
            optohybrid->broadcastWrite("CFG_PULSE_STRETCH", m_scanInfo.bag.mspl.value_, vfatMask, false);

            if (m_scanInfo.bag.signalSourceType.value_ < 1) { // Calibration pulses
                // Disable the calibration module and the calibration pulses for all channels
                optohybrid->confCalPulse(link, vfatMask, 128, false, false, 0);

                // Use voltage pulses (CAL_MODE == 0x1)
                // Note that a low CFG_CAL_DAC corresponds to a high injected charge
                optohybrid->broadcastWrite("CFG_CAL_MODE", 0x1, vfatMask, false);
                optohybrid->broadcastWrite("CFG_CAL_DAC", 6, vfatMask, false);

                // Enable only channel 0 on all VFAT
                char reg[100];
                optohybrid->broadcastWrite("VFAT_CHANNELS.CHANNEL0.CALPULSE_ENABLE", 1, vfatMask, false);

                CMSGEMOS_DEBUG("OptoHybridManager::configureAction: reading  specifics for latency on OH0VFAT23 MPSL " << m_scanInfo.bag.mspl.value_ << " reg " << optohybrid->readReg("GEM_AMC.OH.OH0.GEB.VFAT23.CFG_PULSE_STRETCH") << " CAL_DAC " << (uint32_t)optohybrid->readReg("GEM_AMC.OH.OH0.GEB.VFAT23.CFG_CAL_DAC"));
            } else { // Physical particle signal
                // Disable the calibration module
                optohybrid->broadcastWrite("CFG_CAL_MODE", 0x0, vfatMask, false);
            }
        } else if (m_scanInfo.bag.scanType.value_ == 3) { //S-curve
            // FIXME OBSOLETE

            optohybrid->broadcastWrite("CFG_LATENCY", m_scanInfo.bag.latency.value_, vfatMask, false);
            optohybrid->broadcastWrite("CFG_PULSE_STRETCH", m_scanInfo.bag.mspl.value_, vfatMask, false);
            optohybrid->broadcastWrite("CFG_CAL_PHI", m_scanInfo.bag.pulseDelay.value_, vfatMask, false);
            ///for Scurve scans use Current pulse, high CFG_CAL_DAC is a high injected charge amount
            int caldac = 250;
            optohybrid->broadcastWrite("CFG_CAL_DAC", caldac, vfatMask, false);

            uint32_t initialVT1 = m_scanInfo.bag.scanMin.value_; //m_scanMin.value_;

            uint32_t initialVT2 = 0; //std::max(0,(uint32_t)m_scanMax.value_);
        }

        CMSGEMOS_INFO("AMCManager::configureAction: ending specifics for latency ");
    }

    CMSGEMOS_INFO("AMCManager::configureAction end");
}

void gem::hardware::AMCManager::startAction()
{
    CMSGEMOS_INFO("AMCManager::startAction begin");

    // == Set calibration parameters ==
    if (m_scanInfo.bag.scanType.value_ == 2) {
        CMSGEMOS_INFO("AMCManager::startAction() " << std::endl
                                                   << m_scanInfo.bag.toString());
        m_lastLatency = m_scanInfo.bag.scanMin.value_;
        m_lastVT1 = 0;
    } else if (m_scanInfo.bag.scanType.value_ == 3) {
        CMSGEMOS_INFO("AMCManager::startAction() " << std::endl
                                                   << m_scanInfo.bag.toString());
        m_lastLatency = 0;
        m_lastVT1 = m_scanInfo.bag.scanMin.value_;
    }

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);

        if (!optohybrid->isHwConnected()) {
            std::stringstream errmsg;
            errmsg << "AMCManager::startAction: OptoHybrid connected on link " << link << " to AMC in slot " << m_slot.value_ << " is not responding";
            CMSGEMOS_ERROR(errmsg.str());
            XCEPT_RAISE(gem::hardware::managers::optohybrid::exception::Exception, errmsg.str());
        }

        // Work from "front-end" to "back-end"
        // I.e. VFAT first then OptoHybrid FPGA
        const uint32_t vfatMask = m_vfatMask.at(link);
        optohybrid->broadcastWrite("CFG_RUN", 0x1, vfatMask);

        optohybrid->counterReset();
    }

    // == AMC ==
    if (!m_amc->isHwConnected()) {
        std::stringstream errmsg;
        errmsg << "AMCManager::startAction AMC in slot " << m_slot.value_ << " is not connected";
        CMSGEMOS_ERROR(errmsg.str());
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, errmsg.str());
    }

    m_amc->ttcModuleReset();

    // This is the most sensible order
    // 1. Reset, 2. Activation, 3. Start
    m_amc->resetDAQLink();
    m_amc->enableDAQLink();
    m_amc->setL1AEnable(true);

    CMSGEMOS_INFO("AMCManager::startAction end");
}

void gem::hardware::AMCManager::pauseAction()
{
    CMSGEMOS_INFO("AMCManager::pauseAction begin");

    // == AMC ==
    if (!m_amc->isHwConnected()) {
        std::stringstream errmsg;
        errmsg << "AMCManager::pauseAction AMC in slot " << m_slot.value_ << " is not connected";
        CMSGEMOS_ERROR(errmsg.str());
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, errmsg.str());
    }

    // Calibration
    if (m_scanInfo.bag.scanType.value_ == 2) {
        uint32_t updatedLatency = m_lastLatency + m_stepSize.value_;
        CMSGEMOS_INFO("AMCManager::pauseAction LatencyScan AMC" << m_slot.value_ << " Latency " << (int)updatedLatency);

        // wait for events to finish building
        while (!m_amc->l1aFIFOIsEmpty()) {
            CMSGEMOS_DEBUG("AMCManager::pauseAction waiting for AMC" << m_slot.value_ << " to finish building events");
            usleep(10);
        }
        CMSGEMOS_DEBUG("AMCManager::pauseAction AMC" << m_slot.value_ << " finished building events, updating run parameter "
                                                     << (int)updatedLatency);
        m_amc->setDAQLinkRunParameter(0x1, updatedLatency);
    } else if (m_scanInfo.bag.scanType.value_ == 3) {
        uint8_t updatedVT1 = m_lastVT1 + m_stepSize.value_;
        uint8_t updatedVT2 = 0; //std::max(0,(int)m_scanInfo.bag.scanMax.value_);
        CMSGEMOS_INFO("AMCManager::pauseAction ThresholdScan AMC" << m_slot.value_ << ""
                                                                  << " VT1 " << (int)updatedVT1
                                                                  << " VT2 " << (int)updatedVT2);

        // wait for events to finish building
        while (!m_amc->l1aFIFOIsEmpty()) {
            CMSGEMOS_DEBUG("AMCManager::pauseAction waiting for AMC" << m_slot.value_ << " to finish building events");
            usleep(10);
        }
        CMSGEMOS_DEBUG("AMCManager::pauseAction finished AMC" << m_slot.value_ << " building events, updating VT1 " << (int)updatedVT1
                                                              << " and VT2 " << (int)updatedVT2);
        m_amc->setDAQLinkRunParameter(0x2, updatedVT1);
        m_amc->setDAQLinkRunParameter(0x3, updatedVT2);
    }

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);
    }

    // == Update the scan parameters ==
    if (m_scanInfo.bag.scanType.value_ == 2) {
        CMSGEMOS_INFO("AMCManager::pauseAction LatencyScan old Latency " << (int)m_lastLatency);
        m_lastLatency += m_stepSize.value_;
        CMSGEMOS_INFO("AMCManager::pauseAction LatencyScan new Latency " << (int)m_lastLatency);
    } else if (m_scanInfo.bag.scanType.value_ == 3) {
        CMSGEMOS_INFO("AMCManager::pauseAction ThresholdScan old VT1 " << (int)m_lastVT1);
        m_lastVT1 += m_stepSize.value_;
        CMSGEMOS_INFO("AMCManager::pauseAction ThresholdScan new VT1 " << (int)m_lastVT1);
    }

    CMSGEMOS_INFO("AMCManager::pauseAction end");
}

void gem::hardware::AMCManager::resumeAction()
{
    CMSGEMOS_INFO("AMCManager::resumeAction begin");

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);
    }

    // == AMC ==

    CMSGEMOS_INFO("AMCManager::resumeAction end");
}

void gem::hardware::AMCManager::stopAction()
{
    CMSGEMOS_INFO("AMCManager::stopAction begin");

    // == OptoHybrid ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);

        if (!optohybrid->isHwConnected()) {
            std::stringstream errmsg;
            errmsg << "AMCManager::stopAction: OptoHybrid connected on link " << link << " to AMC in slot " << m_slot.value_ << " is not responding";
            CMSGEMOS_ERROR(errmsg.str());
            XCEPT_RAISE(gem::hardware::managers::optohybrid::exception::Exception, errmsg.str());
        }

        const uint32_t vfatMask = m_vfatMask.at(link);
        optohybrid->broadcastWrite("CFG_RUN", 0x0, vfatMask);
    }

    // == AMC ==
    if (!m_amc->isHwConnected()) {
        std::stringstream errmsg;
        errmsg << "AMCManager::stopAction AMC in slot " << m_slot.value_ << " is not connected";
        CMSGEMOS_ERROR(errmsg.str());
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, errmsg.str());
    }

    m_amc->setL1AEnable(false);
    m_amc->disableDAQLink();
    m_amc->resetDAQLink();

    CMSGEMOS_INFO("AMCManager::stopAction end");
}

void gem::hardware::AMCManager::haltAction()
{
    CMSGEMOS_INFO("AMCManager::haltAction begin");

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);
    }

    // == AMC ==

    CMSGEMOS_INFO("AMCManager::haltAction end");
}

void gem::hardware::AMCManager::resetAction()
{
    CMSGEMOS_INFO("AMCManager::resetAction begin");

    // == AMC ==

    // == OptoHybrids ==
    for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
        if (!((m_optohybridMask.value_ >> link) & 0x1))
            continue;

        auto&& optohybrid = m_optohybrids.at(link);
    }

    CMSGEMOS_INFO("AMCManager::resetAction end");
}

xoap::MessageReference gem::hardware::AMCManager::setRunTypeInterCalib(xoap::MessageReference msg)
{
    const std::string commandName = "setRunTypeInterCalib";
    try {
        if (m_amc->isHwConnected()) {
            // ///should it wait for the event to finish building : CG?????
            while (!m_amc->l1aFIFOIsEmpty()) {
                CMSGEMOS_DEBUG("AMCManager::setRunTypeInterCalib waiting for AMC" << m_slot.value_ << " to finish building events");
                usleep(10);
            }
            if (m_scanInfo.bag.scanType.value_ == 2) {

                m_amc->setDAQLinkRunType(0x0);

            } else {
                CMSGEMOS_ERROR("AMCManager::setRunTypeInterCalib AMC in slot " << m_slot.value_ << " is not connected");
            }
        }

        CMSGEMOS_INFO("AMCManager::setRunTypeInterCalib done");
        return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "Done");
    } catch (xcept::Exception& err) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s'",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s: %s.", msgBase.c_str(), xcept::stdformat_exception_history(err).c_str()));
    }

    return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "NotDone");
}

xoap::MessageReference gem::hardware::AMCManager::updateRunParamCalib(xoap::MessageReference msg)
{
    const std::string commandName = "updateRunParamCalib";
    try {
        if (m_amc->isHwConnected()) {
            CMSGEMOS_DEBUG("connected a card in slot " << m_slot.value_);

            if (m_scanInfo.bag.scanType.value_ == 2) {
                // ///should it wait for the event to finish building : CG?????
                while (!m_amc->l1aFIFOIsEmpty()) {
                    CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib waiting for AMC" << m_slot.value_ << " to finish building events");
                    usleep(10);
                }

                uint32_t updatedLatency = m_lastLatency + m_scanInfo.bag.stepSize.value_;

                CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib AMC" << m_slot.value_ << " finished building events, updating run parameter "
                                                                     << (int)updatedLatency);
                //updating the runparam as descrbed here: https://github.com/cms-gem-daq-project/cmsgemos/pull/302
                uint32_t runParams = ((0x1 << 22) | (0x0 << 21) | (0x6 << 13) | ((m_scanInfo.bag.mspl.value_ & 0x7) << 10) | (updatedLatency & 0x3ff));

                m_amc->setDAQLinkRunType(0x2);
                m_amc->setDAQLinkRunParameters(runParams);

            } else if (m_scanInfo.bag.scanType.value_ == 3) {
                uint8_t updatedVT1 = m_lastVT1 + m_scanInfo.bag.stepSize.value_;
                uint8_t updatedVT2 = 0; //std::max(0,(int)m_scanInfo.bag.scanMax.value_);
                CMSGEMOS_INFO("AMCManager::updateRunParamCalib ThresholdScan AMC" << m_slot.value_ << ""
                                                                                  << " VT1 " << (int)updatedVT1
                                                                                  << " VT2 " << (int)updatedVT2);

                // wait for events to finish building
                while (!m_amc->l1aFIFOIsEmpty()) {
                    CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib waiting for AMC" << m_slot.value_ << " to finish building events");
                    usleep(10);
                }
                CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib finished AMC" << m_slot.value_ << " building events, updating VT1 " << (int)updatedVT1
                                                                              << " and VT2 " << (int)updatedVT2);
                m_amc->setDAQLinkRunParameter(0x2, updatedVT1);
                m_amc->setDAQLinkRunParameter(0x3, updatedVT2);
            }
        } else {
            std::stringstream msg;
            msg << "AMCManager::updateRunParamCalib AMC in slot " << m_slot.value_ << " is not connected";
            CMSGEMOS_ERROR(msg.str());
        }

        // Update the scan parameters
        if (m_scanInfo.bag.scanType.value_ == 2) {
            CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib LatencyScan old Latency " << (int)m_lastLatency);
            m_lastLatency += m_scanInfo.bag.stepSize.value_;
            CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib LatencyScan new Latency " << (int)m_lastLatency);
        } else if (m_scanInfo.bag.scanType.value_ == 3) {
            CMSGEMOS_INFO("AMCManager::ScanValue ThresholdScan old VT1 " << (int)m_lastVT1);
            m_lastVT1 += m_scanInfo.bag.stepSize.value_;
            CMSGEMOS_DEBUG("AMCManager::updateRunParamCalib ThresholdScan new VT1 " << (int)m_lastVT1);
        }

        CMSGEMOS_INFO("AMCManager::updateRunParamCalib done");
        return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "Done");
    } catch (xcept::Exception& err) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s'",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s: %s.", msgBase.c_str(), xcept::stdformat_exception_history(err).c_str()));
        XCEPT_DECLARE_NESTED(gem::base::utils::exception::SoftwareProblem,
            top, toolbox::toString("%s.", msgBase.c_str()), err);
        XCEPT_RETHROW(xoap::exception::Exception, msgBase, err);
    }

    XCEPT_RAISE(xoap::exception::Exception, "command not found");
    return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "NotDone");
}

xoap::MessageReference gem::hardware::AMCManager::updateScanValueCalib(xoap::MessageReference msg)
{
    std::string commandName = "updateScanValueCalib";
    CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib");
    try {
        for (size_t link = 0; link < MAX_OPTOHYBRIDS_PER_AMC; ++link) {
            if (!((m_optohybridMask.value_ >> link) & 0x1))
                continue;

            auto&& optohybrid = m_optohybrids.at(link);

            if (optohybrid->isHwConnected()) {
                // turn on all VFATs? or should they always be on?
                const uint32_t vfatMask = m_vfatMask.at(link);
                if (m_scanInfo.bag.scanType.value_ == 2) {
                    uint32_t updatedLatency = m_lastLatency + m_stepSize.value_;
                    CMSGEMOS_DEBUG("AMCManager::updateScanValueCalib LatencyScan OptoHybrid on link " << static_cast<uint32_t>(link)
                                                                                                      << " AMC slot " << m_slot.value_ << " Latency  " << static_cast<uint32_t>(updatedLatency));

                    optohybrid->broadcastWrite("CFG_LATENCY", updatedLatency, vfatMask);
                } else if (m_scanInfo.bag.scanType.value_ == 3) { // FIXME OBSOLETE
                    uint8_t updatedVT1 = m_lastVT1 + m_scanInfo.bag.stepSize.value_;
                    uint8_t VT2 = 0; // std::max(0,static_cast<uint32_t>(m_scanMax.value_));
                    CMSGEMOS_INFO("AMCManager::ThresholdScan OptoHybrid on link " << static_cast<uint32_t>(link)
                                                                                  << " AMC slot " << m_slot.value_ << " VT1 " << static_cast<uint32_t>(updatedVT1)
                                                                                  << " VT2 " << VT2 << " StepSize " << m_scanInfo.bag.stepSize.value_);

                    // optohybrid->broadcastWrite("CFG_THR_ARM_DAC", updatedVT1, vfatMask);
                    // optohybrid->broadcastWrite("CFG_THR_ZCC_DAC", VT2, vfatMask);
                }
                // what resets to do
            } else {
                std::stringstream errmsg;
                errmsg << "AMCManager::updateScanValueCalib OptoHybrid connected on link " << static_cast<uint32_t>(link)
                       << " to AMC in slot " << m_slot.value_ << " is not responding";
                CMSGEMOS_ERROR(errmsg.str());
                // fireEvent("Fail");
                XCEPT_RAISE(gem::hardware::managers::optohybrid::exception::Exception, errmsg.str());
            }
        }

        CMSGEMOS_INFO("AMCManager::updateScanValueCalib end");

        return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "Done");

    } catch (xcept::Exception& err) {
        std::string msgBase = toolbox::toString("Failed to create SOAP reply for command '%s'",
            commandName.c_str());
        CMSGEMOS_ERROR(toolbox::toString("%s: %s.", msgBase.c_str(), xcept::stdformat_exception_history(err).c_str()));
    }
    return gem::utils::soap::GEMSOAPToolBox::makeSOAPReply(commandName, "NotDone");
}
