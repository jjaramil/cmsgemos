/// @file AMCManagerWeb.cpp

#include "gem/hardware/managers/AMCManagerWeb.h"

#include "gem/hardware/devices/exception/Exception.h"
#include "gem/hardware/managers/AMCManager.h"

#include "xcept/tools.h"

#include <memory>

gem::hardware::AMCManagerWeb::AMCManagerWeb(gem::hardware::AMCManager* amcApp)
    : gem::base::GEMWebApplication(amcApp)
{
}

gem::hardware::AMCManagerWeb::~AMCManagerWeb()
{
}

void gem::hardware::AMCManagerWeb::webDefault(xgi::Input* in, xgi::Output* out)
{
    if (p_gemFSMApp)
        CMSGEMOS_DEBUG("current state is" << dynamic_cast<gem::hardware::AMCManager*>(p_gemFSMApp)->getCurrentState());

    *out << cgicc::style().set("type", "text/css").set("src", "/gemdaq/gemhardware/html/css/optohybrid/optohybrid.css")
         << cgicc::style() << std::endl;

    *out << cgicc::script().set("type", "text/javascript").set("src", "/gemdaq/gemhardware/html/scripts/amc/amc.js")
         << cgicc::script() << std::endl;

    *out << cgicc::script().set("type", "text/javascript").set("src", "/gemdaq/gemhardware/html/scripts/optohybrid/optohybrid.js")
         << cgicc::script() << std::endl;

    GEMWebApplication::webDefault(in, out);
}

void gem::hardware::AMCManagerWeb::expertPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("AMCManagerWeb::expertPage");

    *out << "    <div class=\"xdaq-tab-wrapper\">" << std::endl;
    *out << "      <div class=\"xdaq-tab\" title=\"Register dump page\"/>" << std::endl;
    registerDumpPage(in, out);
    *out << "      </div>" << std::endl;
    *out << "    </div>" << std::endl;
}

void gem::hardware::AMCManagerWeb::applicationPage(xgi::Input* in, xgi::Output* out)
{
    std::string cardURL = "/" + p_gemApp->getApplicationDescriptor()->getURN() + "/cardPage";
    *out << "  <div class=\"xdaq-tab\" title=\"Card page\"/>" << std::endl;
    /* cardPage(in, out); */
    *out << "  </div>" << std::endl;
}

void gem::hardware::AMCManagerWeb::registerDumpPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("AMCManagerWeb::registerDumpPage");
}

void gem::hardware::AMCManagerWeb::monitorPage(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("AMCManagerWeb::monitorPage");
}

void gem::hardware::AMCManagerWeb::jsonUpdate(xgi::Input* in, xgi::Output* out)
{
    CMSGEMOS_DEBUG("AMCManagerWeb::jsonUpdate");
}
