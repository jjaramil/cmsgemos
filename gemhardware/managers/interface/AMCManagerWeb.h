/// @file AMCManagerWeb.h

#ifndef GEM_HARDWARE_AMCMANAGERWEB_H
#define GEM_HARDWARE_AMCMANAGERWEB_H

#include "gem/base/GEMWebApplication.h"

namespace gem {
namespace hardware {

    class AMCManager;

    class AMCManagerWeb : public gem::base::GEMWebApplication {
        friend class AMCManager;

    public:
        AMCManagerWeb(AMCManager* amcApp);
        virtual ~AMCManagerWeb();

    protected:
        void webDefault(xgi::Input* in, xgi::Output* out) override;
        void monitorPage(xgi::Input* in, xgi::Output* out) override;
        void expertPage(xgi::Input* in, xgi::Output* out) override;
        void applicationPage(xgi::Input* in, xgi::Output* out) override;
        void jsonUpdate(xgi::Input* in, xgi::Output* out) override;

        void registerDumpPage(xgi::Input* in, xgi::Output* out);
    };

} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_AMCMANAGERWEB_H
