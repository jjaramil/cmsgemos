/// @file version.h

#ifndef GEM_HARDWARE_MANAGERS_VERSION_H
#define GEM_HARDWARE_MANAGERS_VERSION_H

#ifndef DOXYGEN_IGNORE_THIS

#include "config/PackageInfo.h"

namespace gem {
namespace hardware {
    namespace managers {

#define GEMHARDWAREMANAGERS_VERSION_MAJOR 1
#define GEMHARDWAREMANAGERS_VERSION_MINOR 0
#define GEMHARDWAREMANAGERS_VERSION_PATCH 1
#define GEMHARDWAREMANAGERS_PREVIOUS_VERSIONS "0.0.0,0.1.0,0.2.0,0.2.1,0.2.2,0.2.3,0.3.0,0.3.1,0.3.2,0.4.0,0.99.0,0.99.1,1.0.0"

#define GEMHARDWAREMANAGERS_VERSION_CODE PACKAGE_VERSION_CODE(GEMHARDWAREMANAGERS_VERSION_MAJOR, GEMHARDWAREMANAGERS_VERSION_MINOR, GEMHARDWAREMANAGERS_VERSION_PATCH)
#ifndef GEMHARDWAREMANAGERS_PREVIOUS_VERSIONS
#define GEMHARDWAREMANAGERS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(GEMHARDWAREMANAGERS_VERSION_MAJOR, GEMHARDWAREMANAGERS_VERSION_MINOR, GEMHARDWAREMANAGERS_VERSION_PATCH)
#else
#define GEMHARDWAREMANAGERS_FULL_VERSION_LIST GEMHARDWAREMANAGERS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(GEMHARDWAREMANAGERS_VERSION_MAJOR, GEMHARDWAREMANAGERS_VERSION_MINOR, GEMHARDWAREMANAGERS_VERSION_PATCH)
#endif

        const std::string project = "cmsgemos";
        const std::string package = "hardware::managers";
        const std::string versions = GEMHARDWAREMANAGERS_FULL_VERSION_LIST;
        const std::string summary = "Hardware interfaces for GEM managers";
        const std::string description = "";
        const std::string authors = "GEM Online Systems Group";
        const std::string link = "https://cms-gem-daq-project.github.io/cmsgemos/";

        config::PackageInfo getPackageInfo();
        void checkPackageDependencies();
        std::set<std::string, std::less<std::string>> getPackageDependencies();
    }
}
}

#endif // DOXYGEN_IGNORE_THIS

#endif // GEM_HARDWARE_VERSION_H
