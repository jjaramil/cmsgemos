/// @file AMCManager.h

#ifndef GEM_HARDWARE_AMCMANAGER_H
#define GEM_HARDWARE_AMCMANAGER_H

#include "gem/base/GEMFSMApplication.h"

#include "xdata/Boolean.h"
#include "xdata/UnsignedInteger32.h"

#include <memory>

namespace gem {
namespace hardware {

    class AMCManagerWeb;
    namespace amc {
        class HwGenericAMC;
    }
    namespace optohybrid {
        class HwOptoHybrid;
    }

    using amc_shared_ptr = std::shared_ptr<gem::hardware::amc::HwGenericAMC>;
    using optohybrid_shared_ptr = std::shared_ptr<gem::hardware::optohybrid::HwOptoHybrid>;

    class AMCManager : public gem::base::GEMFSMApplication {

        friend class AMCManagerWeb;

    public:
        XDAQ_INSTANTIATOR();

        AMCManager(xdaq::ApplicationStub* s);
        virtual ~AMCManager();

    protected:
        void init();

        void actionPerformed(xdata::Event& event) override;

        // State transitions
        void initializeAction() override;
        void configureAction() override;
        void startAction() override;
        void pauseAction() override;
        void resumeAction() override;
        void stopAction() override;
        void haltAction() override;
        void resetAction() override;

    private:
        // Hardware description
        xdata::UnsignedInteger32 m_crate = 0; ///< Specifies the crate in which the AMC is located
        xdata::UnsignedInteger32 m_slot = 0; ///< Specifies the slot in which the AMC is located
        xdata::UnsignedInteger32 m_optohybridMask = 0x0; ///< OptoHybrids to use in the run

        // Configuration
        xdata::Boolean m_enableZS = false; ///< Enable the zero-suppression
        xdata::Boolean m_autoKillOptoHybrid = false; ///< Automatically disable non-communicating OptoHybrids during the configuration step

        amc_shared_ptr m_amc; ///< AMC hardware object
        std::array<optohybrid_shared_ptr, MAX_OPTOHYBRIDS_PER_AMC> m_optohybrids; ///< OptoHybrid hardware objects
        std::array<uint32_t, MAX_OPTOHYBRIDS_PER_AMC> m_vfatMask; ///< Automatically determined VFAT mask

        // Calibration scans
        uint32_t m_lastLatency; ///< Special variable for latency scan mode
        uint32_t m_lastVT1, m_lastVT2; ///< Special variable for threshold scan mode

        xoap::MessageReference setRunTypeInterCalib(xoap::MessageReference msg);
        xoap::MessageReference updateRunParamCalib(xoap::MessageReference msg);
        xoap::MessageReference updateScanValueCalib(xoap::MessageReference msg);

    }; // class AMCManager
} // namespace gem::hardware
} // namespace gem

#endif // GEM_HARDWARE_AMCMANAGER_H
