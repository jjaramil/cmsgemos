#include "gem/hardware/devices/amc/HwGenericAMC.h"
#include <gem/rpc/amc.h>
#include <gem/rpc/amc/daq.h>
#include <gem/rpc/amc/sca.h>
#include <gem/rpc/amc/ttc.h>
#include <gem/rpc/daq_monitor.h>
#include <gem/rpc/gbt.h>

#include <chrono>
#include <fstream>
#include <iomanip>
#include <numeric>
#include <string>
#include <thread>
#include <vector>

gem::hardware::amc::HwGenericAMC::HwGenericAMC(std::string const& amcDevice)
    : gem::hardware::GEMHwDevice::GEMHwDevice(amcDevice)
    , m_maxLinks(gem::hardware::utils::N_GTX)
{
    CMSGEMOS_INFO("HwGenericAMC ctor");
    this->setup(amcDevice);
    CMSGEMOS_INFO("HwGenericAMC ctor done");
}

gem::hardware::amc::HwGenericAMC::~HwGenericAMC()
{
}

void gem::hardware::amc::HwGenericAMC::connectRPC(bool reconnect)
{
    if (isConnected) {
        // TODO: find better way than hardcoded versions
        this->gem::hardware::GEMHwDevice::connectRPC(); //FIXME required as atomic transactions are still used here
        this->loadModule("amc", "amc v1.0.1");
        this->loadModule("gbt", "gbt v1.0.1");
        this->loadModule("daq_monitor", "daq_monitor v1.0.1");
        CMSGEMOS_DEBUG("HwGenericAMC::connectRPC modules loaded");
    } else {
        CMSGEMOS_WARN("HwGenericAMC::connectRPC RPC interface failed to connect");
    }
}

bool gem::hardware::amc::HwGenericAMC::isHwConnected()
{
    // DO NOT LIKE THIS FUNCTION FIXME!!!
    if (b_is_connected) {
        CMSGEMOS_INFO("basic check: HwGenericAMC connection good");
        return true;
    } else if (gem::hardware::GEMHwDevice::isHwConnected()) {
        CMSGEMOS_INFO("basic check: HwGenericAMC pointer valid");
        m_maxLinks = this->getSupportedOptoHybrids();
        // FIXME needs update for V3
        if (((this->getBoardIDString()).rfind("GLIB") != std::string::npos) || ((this->getBoardIDString()).rfind("beef") != std::string::npos)) {
            CMSGEMOS_INFO("HwGenericAMC found boardID");

            return true;
        } else {
            CMSGEMOS_WARN("Device not reachable (unable to find 'GLIB' in the board ID)"
                << " board ID " << this->getBoardIDString()
                << " user firmware version " << this->getFirmwareVer());

            return false;
        }
    } else {
        return false;
    }
}

std::string gem::hardware::amc::HwGenericAMC::getBoardIDString()
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    // The board ID consists of four characters encoded as a 32-bit unsigned int
    std::string res = "N/A";
    uint32_t brdID = getBoardID();
    res = toolbox::toString("%x", brdID);
    return res;
}

uint32_t gem::hardware::amc::HwGenericAMC::getBoardID()
{
    return readReg("GEM_AMC.GEM_SYSTEM.BOARD_ID");
}

std::string gem::hardware::amc::HwGenericAMC::getBoardTypeString()
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    // The board ID consists of four characters encoded as a 32-bit unsigned int
    std::string res = "N/A";
    uint32_t brdID = getBoardType();
    res = toolbox::toString("%x", brdID);
    return res;
}

uint32_t gem::hardware::amc::HwGenericAMC::getBoardType()
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    // The board type consists of four characters encoded as a 32-bit unsigned int
    return readReg("GEM_AMC.GEM_SYSTEM.BOARD_TYPE");
}

std::string gem::hardware::amc::HwGenericAMC::getSystemIDString()
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    // The system ID consists of four characters encoded as a 32-bit unsigned int
    std::string res = "N/A";
    uint32_t sysID = getSystemID();
    res = toolbox::toString("%x", sysID);
    return res;
}

uint32_t gem::hardware::amc::HwGenericAMC::getSystemID()
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    // The system ID consists of four characters encoded as a 32-bit unsigned int
    return readReg("GEM_AMC.GEM_SYSTEM.BOARD_TYPE");
}

uint32_t gem::hardware::amc::HwGenericAMC::getSupportedOptoHybrids()
{
    return readReg("GEM_AMC.GEM_SYSTEM.CONFIG.NUM_OF_OH");
}

uint32_t gem::hardware::amc::HwGenericAMC::supportsTriggerLink()
{
    return readReg("GEM_AMC.GEM_SYSTEM.CONFIG.USE_TRIG_LINKS");
}

std::string gem::hardware::amc::HwGenericAMC::getFirmwareDateString(bool const& system)
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    std::stringstream res;
    std::stringstream regName;
    uint32_t fwid = getFirmwareDate(system);

    // GLIB system FW 0x1f0222b7
    res << std::setfill('0') << std::setw(2) << (fwid & 0x1f) // day
        << "-" << std::setfill('0') << std::setw(2) << ((fwid >> 5) & 0x0f) // month
        << "-" << std::setw(4) << 2000 + ((fwid >> 9) & 0x7f); // year
    // GEM system FW date 0xYYYYMMDD
    return res.str();
}

uint32_t gem::hardware::amc::HwGenericAMC::getFirmwareDate(bool const& system)
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    if (system)
        return readReg("GEM_AMC.GEM_SYSTEM.RELEASE.DATE");
    else
        return readReg("GEM_AMC.GEM_SYSTEM.RELEASE.DATE");
}

std::string gem::hardware::amc::HwGenericAMC::getFirmwareVerString(bool const& system)
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    std::stringstream res;
    std::stringstream regName;
    uint32_t fwid;

    // GLIB system FW 0x1f0222b7
    fwid = getFirmwareVer(system);
    res << ((fwid >> 16) & 0xff) << "."
        << ((fwid >> 8) & 0xff) << "."
        << ((fwid)&0xff);
    // GEM system FW version 0xMMmmpppp
    return res.str();
}

uint32_t gem::hardware::amc::HwGenericAMC::getFirmwareVer(bool const& system)
{
    // gem::utils::LockGuard<gem::utils::Lock> guardedLock(hwLock_);
    if (system)
        return readReg("GEM_AMC.GEM_SYSTEM.RELEASE");
    else
        return readReg("GEM_AMC.GEM_SYSTEM.RELEASE");
}

/// User core functionality
uint32_t gem::hardware::amc::HwGenericAMC::getUserFirmware()
{
    // This returns the firmware register (V2 removed the user firmware specific).
    return readReg("GEM_AMC.GEM_SYSTEM.RELEASE");
}

std::string gem::hardware::amc::HwGenericAMC::getUserFirmwareDate()
{
    // This returns the user firmware build date.
    std::stringstream res;
    res << "0x" << std::hex << getUserFirmware() << std::dec;
    return res.str();
}

uint32_t gem::hardware::amc::HwGenericAMC::readTriggerFIFO(uint8_t const& gtx)
{
    // V2 firmware hasn't got trigger fifo yet
    return 0;
}

void gem::hardware::amc::HwGenericAMC::flushTriggerFIFO(uint8_t const& gtx)
{
    // V2 firmware hasn't got trigger fifo yet
    return;
}

/// DAQ link module functions
void gem::hardware::amc::HwGenericAMC::configureDAQModule(const uint32_t inputEnableMask, const bool enableZS, const uint32_t davTO, const uint32_t ttsOverride)
{
    try {
        xhal::client::call<::amc::daq::configureDAQModule>(rpc, inputEnableMask, enableZS, davTO, ttsOverride);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::configureDAQModule", gem::hardware::devices::exception::Exception);
}

void gem::hardware::amc::HwGenericAMC::enableDAQLink()
//FIXME the meaning of the remote method has changed!!!
{
    try {
        xhal::client::call<::amc::daq::enableDAQLink>(rpc);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::enableDAQLink", gem::hardware::devices::exception::Exception);
}

void gem::hardware::amc::HwGenericAMC::disableDAQLink()
{
    try {
        xhal::client::call<::amc::daq::disableDAQLink>(rpc);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::disableDAQLink", gem::hardware::devices::exception::Exception);
}

void gem::hardware::amc::HwGenericAMC::setZS(bool en)
{
    writeReg("GEM_AMC.DAQ.CONTROL.ZERO_SUPPRESSION_EN", uint32_t(en));
}

void gem::hardware::amc::HwGenericAMC::resetDAQLink()
{
    try {
        xhal::client::call<::amc::daq::resetDAQLink>(rpc);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::resetDAQLink", gem::hardware::devices::exception::Exception);
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkControl()
{
    return readReg("GEM_AMC.DAQ.CONTROL");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkStatus()
{
    return readReg("GEM_AMC.DAQ.STATUS");
}

bool gem::hardware::amc::HwGenericAMC::daqLinkReady()
{
    return readReg("GEM_AMC.DAQ.STATUS.DAQ_LINK_RDY");
}

bool gem::hardware::amc::HwGenericAMC::daqClockLocked()
{
    return readReg("GEM_AMC.DAQ.STATUS.DAQ_CLK_LOCKED");
}

bool gem::hardware::amc::HwGenericAMC::daqTTCReady()
{
    return readReg("GEM_AMC.DAQ.STATUS.TTC_RDY");
}

uint8_t gem::hardware::amc::HwGenericAMC::daqTTSState()
{
    return readReg("GEM_AMC.DAQ.STATUS.TTS_STATE");
}

bool gem::hardware::amc::HwGenericAMC::daqAlmostFull()
{
    return readReg("GEM_AMC.DAQ.STATUS.DAQ_AFULL");
}

bool gem::hardware::amc::HwGenericAMC::l1aFIFOIsEmpty()
{
    return readReg("GEM_AMC.DAQ.STATUS.L1A_FIFO_IS_EMPTY");
}

bool gem::hardware::amc::HwGenericAMC::l1aFIFOIsAlmostFull()
{
    return readReg("GEM_AMC.DAQ.STATUS.L1A_FIFO_IS_NEAR_FULL");
}

bool gem::hardware::amc::HwGenericAMC::l1aFIFOIsFull()
{
    return readReg("GEM_AMC.DAQ.STATUS.L1A_FIFO_IS_FULL");
}

bool gem::hardware::amc::HwGenericAMC::l1aFIFOIsUnderflow()
{
    return readReg("GEM_AMC.DAQ.STATUS.L1A_FIFO_IS_UNDERFLOW");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkEventsSent()
{
    return readReg("GEM_AMC.DAQ.EXT_STATUS.EVT_SENT");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkL1AID()
{
    return readReg("GEM_AMC.DAQ.EXT_STATUS.L1AID");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkDisperErrors()
{
    return readReg("GEM_AMC.DAQ.EXT_STATUS.DISPER_ERR");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkNonidentifiableErrors()
{
    return readReg("GEM_AMC.DAQ.EXT_STATUS.NOTINTABLE_ERR");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkInputMask()
{
    return readReg("GEM_AMC.DAQ.CONTROL.INPUT_ENABLE_MASK");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkDAVTimeout()
{
    return readReg("GEM_AMC.DAQ.CONTROL.DAV_TIMEOUT");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkDAVTimer(bool const& max)
{
    if (max)
        return readReg("GEM_AMC.DAQ.EXT_STATUS.MAX_DAV_TIMER");
    else
        return readReg("GEM_AMC.DAQ.EXT_STATUS.LAST_DAV_TIMER");
}

/// GTX specific DAQ link information
// TODO: should rename, "DAQ link" is a back end FW term, not corresponding to a front end link...
uint32_t gem::hardware::amc::HwGenericAMC::getLinkDAQStatus(uint8_t const& gtx)
{
    // do link protections here...
    std::stringstream regBase;
    regBase << "DAQ.OH" << (int)gtx;
    return readReg("GEM_AMC." + regBase.str() + ".STATUS");
}

uint32_t gem::hardware::amc::HwGenericAMC::getLinkDAQCounters(uint8_t const& gtx, uint8_t const& mode)
{
    std::stringstream regBase;
    regBase << "DAQ.OH" << (int)gtx << ".COUNTERS";
    if (mode == 0)
        return readReg("GEM_AMC." + regBase.str() + ".CORRUPT_VFAT_BLK_CNT");
    else
        return readReg("GEM_AMC." + regBase.str() + ".EVN");
}

uint32_t gem::hardware::amc::HwGenericAMC::getLinkLastDAQBlock(uint8_t const& gtx)
{
    std::stringstream regBase;
    regBase << "DAQ.OH" << (int)gtx;
    return readReg("GEM_AMC." + regBase.str() + ".LASTBLOCK");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkInputTimeout()
{
    // OBSOLETE, NO LONGER PRESENT
    return readReg("GEM_AMC.DAQ.EXT_CONTROL.INPUT_TIMEOUT");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkRunType()
{
    return readReg("GEM_AMC.DAQ.EXT_CONTROL.RUN_TYPE");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkRunParameters()
{
    return readReg("GEM_AMC.DAQ.EXT_CONTROL.RUN_PARAMS");
}

uint32_t gem::hardware::amc::HwGenericAMC::getDAQLinkRunParameter(uint8_t const& parameter)
{
    std::stringstream regBase;
    regBase << "DAQ.EXT_CONTROL.RUN_PARAM" << (int)parameter;
    return readReg("GEM_AMC." + regBase.str());
}

void gem::hardware::amc::HwGenericAMC::setDAQLinkInputTimeout(uint32_t const& value)
{
    // for (unsigned li = 0; li < m_maxLinks; ++li) {
    // for (unsigned li =  m_maxLinks - 1; li > -1; --li) {
    //   writeReg("GEM_AMC."+toolbox::toString("DAQ.OH%d.CONTROL.EOE_TIMEOUT", li), value);
    // }
    // return writeReg("GEM_AMC.DAQ.EXT_CONTROL.INPUT_TIMEOUT",value);
}

void gem::hardware::amc::HwGenericAMC::setDAQLinkRunType(uint32_t const& value)
{
    return writeReg("GEM_AMC.DAQ.EXT_CONTROL.RUN_TYPE", value);
}

void gem::hardware::amc::HwGenericAMC::setDAQLinkRunParameters(uint32_t const& value)
{
    return writeReg("GEM_AMC.DAQ.EXT_CONTROL.RUN_PARAMS", value);
}

void gem::hardware::amc::HwGenericAMC::setDAQLinkRunParameter(uint8_t const& parameter, uint8_t const& value)
{
    if (parameter < 1 || parameter > 3) {
        std::string msg = toolbox::toString("Attempting to set DAQ link run parameter %d: outside expectation (1-%d)",
            (int)parameter, 3);
        CMSGEMOS_ERROR(msg);
        return;
    }
    std::stringstream regBase;
    regBase << "DAQ.EXT_CONTROL.RUN_PARAM" << (int)parameter;
    writeReg("GEM_AMC." + regBase.str(), value);
}

/// TTC module information

/// TTC module functions
void gem::hardware::amc::HwGenericAMC::ttcModuleReset()
{
    writeReg("GEM_AMC.TTC.CTRL.MODULE_RESET", 0x1);
}

void gem::hardware::amc::HwGenericAMC::ttcMMCMReset()
{
    writeReg("GEM_AMC.TTC.CTRL.MMCM_RESET", 0x1);
    // writeReg("GEM_AMC.TTC.CTRL.PHASE_ALIGNMENT_RESET", 0x1);
}

void gem::hardware::amc::HwGenericAMC::ttcMMCMPhaseShift(bool relock, bool modeBC0, bool scan)
{
    try {
        xhal::client::call<::amc::ttc::ttcMMCMPhaseShift>(rpc, relock, modeBC0, scan);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::ttcMMCMPhaseShift", gem::hardware::devices::exception::Exception);
}

int gem::hardware::amc::HwGenericAMC::checkPLLLock(uint32_t readAttempts)
{
    try {
        return xhal::client::call<::amc::ttc::checkPLLLock>(rpc, readAttempts);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::checkPLLLock", gem::hardware::devices::exception::Exception);
}

double gem::hardware::amc::HwGenericAMC::getMMCMPhaseMean(uint32_t readAttempts)
{
    float res = 0.;
    try {
        res = xhal::client::call<::amc::ttc::getMMCMPhaseMean>(rpc, readAttempts);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::getMMCMPhaseMean", gem::hardware::devices::exception::Exception);
    return static_cast<double>(res);
}

double gem::hardware::amc::HwGenericAMC::getMMCMPhaseMedian(uint32_t readAttempts)
{
    CMSGEMOS_WARN("HwGenericAMC::getMMCMPhaseMedian is not implemented, returning the mean");
    return getMMCMPhaseMean(readAttempts);
}

double gem::hardware::amc::HwGenericAMC::getGTHPhaseMean(uint32_t readAttempts)
{
    float res = 0.;
    try {
        res = xhal::client::call<::amc::ttc::getGTHPhaseMean>(rpc, readAttempts);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::getGTHPhaseMean", gem::hardware::devices::exception::Exception);
    return static_cast<double>(res);
}

double gem::hardware::amc::HwGenericAMC::getGTHPhaseMedian(uint32_t readAttempts)
{
    CMSGEMOS_WARN("HwGenericAMC::getGTHPhaseMedian is not implemented, returning the mean");
    return getGTHPhaseMean(readAttempts);
}

void gem::hardware::amc::HwGenericAMC::ttcCounterReset()
{
    writeReg("GEM_AMC.TTC.CTRL.CNT_RESET", 0x1);
}

bool gem::hardware::amc::HwGenericAMC::getL1AEnable()
{
    return readReg("GEM_AMC.TTC.CTRL.L1A_ENABLE");
}

void gem::hardware::amc::HwGenericAMC::setL1AEnable(bool enable)
{
    // uint32_t safeEnable = 0xa4a2c200+int(enable);
    writeReg("GEM_AMC.TTC.CTRL.L1A_ENABLE", uint32_t(enable));
}

uint32_t gem::hardware::amc::HwGenericAMC::getTTCConfig(AMCTTCCommandT const& cmd)
{
    CMSGEMOS_WARN("HwGenericAMC::getTTCConfig: not implemented");
    return 0x0;
}

void gem::hardware::amc::HwGenericAMC::setTTCConfig(AMCTTCCommandT const& cmd, uint8_t const& value)
{
    CMSGEMOS_WARN("HwGenericAMC::setTTCConfig: not implemented");
    return;
}

uint32_t gem::hardware::amc::HwGenericAMC::getTTCStatus()
{
    CMSGEMOS_WARN("HwGenericAMC::getTTCStatus: not fully implemented");
    return readReg("GEM_AMC.TTC.STATUS");
}

uint32_t gem::hardware::amc::HwGenericAMC::getTTCErrorCount(bool const& single)
{
    if (single)
        return readReg("GEM_AMC.TTC.STATUS.TTC_SINGLE_ERROR_CNT");
    else
        return readReg("GEM_AMC.TTC.STATUS.TTC_DOUBLE_ERROR_CNT");
}

uint32_t gem::hardware::amc::HwGenericAMC::getTTCCounter(AMCTTCCommandT const& cmd)
{
    switch (cmd) {
    case (AMCTTCCommand::TTC_L1A):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.L1A");
    case (AMCTTCCommand::TTC_BC0):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.BC0");
    case (AMCTTCCommand::TTC_EC0):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.EC0");
    case (AMCTTCCommand::TTC_RESYNC):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.RESYNC");
    case (AMCTTCCommand::TTC_OC0):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.OC0");
    case (AMCTTCCommand::TTC_HARD_RESET):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.HARD_RESET");
    case (AMCTTCCommand::TTC_CALPULSE):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.CALPULSE");
    case (AMCTTCCommand::TTC_START):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.START");
    case (AMCTTCCommand::TTC_STOP):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.STOP");
    case (AMCTTCCommand::TTC_TEST_SYNC):
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.TEST_SYNC");
    default:
        return readReg("GEM_AMC.TTC.CMD_COUNTERS.L1A");
    }
}

uint32_t gem::hardware::amc::HwGenericAMC::getL1AID()
{
    return readReg("GEM_AMC.TTC.L1A_ID");
}

uint32_t gem::hardware::amc::HwGenericAMC::getL1ARate()
{
    return readReg("GEM_AMC.TTC.L1A_RATE");
}

uint32_t gem::hardware::amc::HwGenericAMC::getTTCSpyBuffer()
{
    // FIXME: OBSOLETE in V3?
    CMSGEMOS_WARN("HwGenericAMC::getTTCSpyBuffer: TTC.TTC_SPY_BUFFER is obsolete and will be removed in a future release");
    return 0x0;
    // return readReg("GEM_AMC.TTC.TTC_SPY_BUFFER");
}

/// SLOW_CONTROL module information

/// SCA submodule
void gem::hardware::amc::HwGenericAMC::scaHardResetEnable(bool const& en)
{
    writeReg("GEM_AMC.SLOW_CONTROL.SCA.CTRL.TTC_HARD_RESET_EN", uint32_t(en));
}

/// TRIGGER module information

void gem::hardware::amc::HwGenericAMC::triggerReset()
{
    writeReg("GEM_AMC.TRIGGER.CTRL.MODULE_RESET", 0x1);
}

void gem::hardware::amc::HwGenericAMC::triggerCounterReset()
{
    writeReg("GEM_AMC.TRIGGER.CTRL.CNT_RESET", 0x1);
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridKillMask()
{
    return readReg("GEM_AMC.TRIGGER.CTRL.OH_KILL_MASK");
}

void gem::hardware::amc::HwGenericAMC::setOptoHybridKillMask(uint32_t const& mask)
{
    writeReg("GEM_AMC.TRIGGER.CTRL.OH_KILL_MASK", mask);
}

/// STATUS submodule
uint32_t gem::hardware::amc::HwGenericAMC::getORTriggerRate()
{
    return readReg("GEM_AMC.TRIGGER.STATUS.OR_TRIGGER_RATE");
}

uint32_t gem::hardware::amc::HwGenericAMC::getORTriggerCount()
{
    return readReg("GEM_AMC.TRIGGER.STATUS.TRIGGER_SINGLE_ERROR_CNT");
}

/// OH{IDXX} submodule
uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridTriggerRate(uint8_t const& oh)
{
    return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.TRIGGER_RATE", (int)oh));
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridTriggerCount(uint8_t const& oh)
{
    return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.TRIGGER_CNT", (int)oh));
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridClusterRate(uint8_t const& oh, uint8_t const& cs)
{
    return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.CLUSTER_SIZE_%d_RATE", (int)oh, (int)cs));
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridClusterCount(uint8_t const& oh, uint8_t const& cs)
{
    return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.CLUSTER_SIZE_%d_CNT", (int)oh, (int)cs));
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridDebugLastCluster(uint8_t const& oh, uint8_t const& cs)
{
    return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.DEBUG_LAST_CLUSTER_%d", (int)oh, (int)cs));
}

uint32_t gem::hardware::amc::HwGenericAMC::getOptoHybridTriggerLinkCount(uint8_t const& oh, uint8_t const& link, AMCOHLinkCountT const& count)
{
    switch (count) {
    case (AMCOHLinkCount::LINK_NOT_VALID):
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_NOT_VALID_CNT", (int)oh, (int)link));
    case (AMCOHLinkCount::LINK_MISSED_COMMA):
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_MISSED_COMMA_CNT", (int)oh, (int)link));
    case (AMCOHLinkCount::LINK_OVERFLOW):
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_OVERFLOW_CNT", (int)oh, (int)link));
    case (AMCOHLinkCount::LINK_UNDERFLOW):
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_UNDERFLOW_CNT", (int)oh, (int)link));
    case (AMCOHLinkCount::LINK_SYNC_WORD):
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_SYNC_WORD_CNT", (int)oh, (int)link));
    default:
        return readReg("GEM_AMC." + toolbox::toString("TRIGGER.OH%d.LINK%d_MISSED_COMMA_CNT", (int)oh, (int)link));
    }
}

// general resets
void gem::hardware::amc::HwGenericAMC::generalReset()
{
    // TODO: CTP7 module candidate
    // reset all counters
    CMSGEMOS_WARN("HwGenericAMC::generalReset: not yet implemented");
    return;
}

void gem::hardware::amc::HwGenericAMC::linkReset(uint8_t const& gtx)
{
    // TODO: CTP7 module candidate
    // req = wisc::RPCMsg("amc.linkReset");
    // req.set_word("NOH", static_cast<uint32_t>(gtx));
    // try {
    //   rsp = rpc.call_method(req);
    // } STANDARD_CATCH;

    // try {
    //   if (rsp.get_key_exists("error")) {
    //     ERROR("LinkReset error: " << rsp.get_string("error").c_str());
    //     //throw xhal::client::utils::XHALException("DAQ_TRIGGER_MAIN update failed");
    //   }
    // } STANDARD_CATCH;

    CMSGEMOS_WARN("HwGenericAMC::linkReset: not yet implemented");
    return;
}

uint32_t gem::hardware::amc::HwGenericAMC::getGBTStatus()
{
    // Check status of all GBTs
    auto gbtStatus = xhal::client::call<::daqmon::getmonGBTLink>(rpc, true);

    uint32_t optohybridMask = 0;
    for (size_t i = 0; i < ::amc::OH_PER_AMC; i++) {
        std::vector<int> gbtReady;
        for (size_t j = 0; j < ::gbt::GBTS_PER_OH; j++) {
            std::string index = "OH" + std::to_string(i) + ".GBT" + std::to_string(j) + ".READY";
            gbtReady.push_back(gbtStatus.find(index)->second);
        }
        // Produce the mask of "good" optical links
        // Require status of all GBTs on a particular link to be "READY"
        if (std::accumulate(gbtReady.begin(), gbtReady.end(), 1, std::multiplies<int>()) > 0)
            optohybridMask += (1 << i);
    }

    return optohybridMask;
}

uint32_t gem::hardware::amc::HwGenericAMC::establishFrontEndCommunication(uint32_t optohybridMask)
{
    // Check the GBT status before programming
    for (size_t i = 0; i < 20; ++i) {
        const uint32_t gbtStatus = getGBTStatus();
        if ((gbtStatus & optohybridMask) == optohybridMask)
            break; // All good!

        const std::string msg = toolbox::toString("Valid links (%d) different than expectations (%d) before programming GBT.", gbtStatus, optohybridMask);
        CMSGEMOS_WARN(msg);

        // Continue with the working OptoHybrids only
        if (i == 19) {
            optohybridMask = gbtStatus & optohybridMask;
        }

        // Let the GBT some time to lock
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    // Call the configureGBTs RPC method for the GBT configuration, i.e. using the configuration files stored on the AMC
    for (size_t i = 0; i < ::amc::OH_PER_AMC; i++) {
        if (((optohybridMask >> i) & 0x1) == 0)
            continue;

        try {
            xhal::client::call<::gbt::configureGBTs>(rpc, i);
        }
        GEM_CATCH_RPC_ERROR("HwGenericAMC::establishFrontEndCommunication", gem::hardware::devices::exception::Exception);
    }

    // Check the GBT status after programming
    const uint32_t gbtStatus = getGBTStatus();
    if ((gbtStatus & optohybridMask) != optohybridMask) {
        const std::string msg = toolbox::toString("Valid links (%d) different than expectations (%d) after programming GBT.", gbtStatus, optohybridMask);
        CMSGEMOS_ERROR(msg);
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, msg);
    }

    // Reset the SCA
    try {
        xhal::client::call<::amc::sca::scaModuleReset>(rpc, optohybridMask);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::establishFrontEndCommunication", gem::hardware::devices::exception::Exception);

    // Get the GEM_LOADER counters
    const uint16_t gemloader_request_counter = readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_REQUEST_CNT");
    const uint16_t gemloader_success_counter = readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_SUCCESS_CNT");
    const uint16_t gemloader_failure_counter = readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_FAIL_CNT");

    // Program the OH FPGA through a local TTC hard reset
    writeReg("GEM_AMC.TTC.GENERATOR.ENABLE", 0x1);
    writeReg("GEM_AMC.TTC.GENERATOR.SINGLE_HARD_RESET", 0x1);

    // Wait for TTC hard reset completion
    std::this_thread::sleep_for(std::chrono::milliseconds(350));

    // Clean-up
    writeReg("GEM_AMC.TTC.GENERATOR.ENABLE", 0x0);
    writeReg("GEM_AMC.GEM_SYSTEM.CTRL.LINK_RESET", 0x1);

    if ((gemloader_request_counter + 1 != readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_REQUEST_CNT")) || (gemloader_success_counter + 1 != readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_SUCCESS_CNT")) || (gemloader_failure_counter != readReg("GEM_AMC.GEM_SYSTEM.GEM_LOADER.LOAD_FAIL_CNT"))) {
        const std::string msg = "GEM_LOADER failed!";
        CMSGEMOS_ERROR(msg);
        XCEPT_RAISE(gem::hardware::devices::exception::Exception, msg);
    }

    return optohybridMask;
}

void gem::hardware::amc::HwGenericAMC::configureAMCCalDataFormat(bool en)
{
    writeReg("GEM_AMC.DAQ.CONTROL.CALIBRATION_MODE_EN", uint32_t(en));

    return;
}

void gem::hardware::amc::HwGenericAMC::recoverAMC()
{
    try {
        xhal::client::call<::amc::recoverAMC>(rpc);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::recoverAMC", gem::hardware::devices::exception::Exception);

    return;
}

void gem::hardware::amc::HwGenericAMC::resetClocks()
{
    try {
        xhal::client::call<::amc::resetClocks>(rpc);
    }
    GEM_CATCH_RPC_ERROR("HwGenericAMC::resetClocks", gem::hardware::devices::exception::Exception);

    return;
}
