#include "toolbox/version.h"
#include "gem/hardware/devices/version.h"
#include "gem/hardware/utils/version.h"
#include "gem/utils/version.h"
#include "xdaq/version.h"

GETPACKAGEINFO(gem::hardware::devices);

void gem::hardware::devices::checkPackageDependencies()
{
    CHECKDEPENDENCY(toolbox);
    CHECKDEPENDENCY(xdaq);
    CHECKDEPENDENCY(gem::utils);
    CHECKDEPENDENCY(gem::hardware::utils);
}

std::set<std::string, std::less<std::string>> gem::hardware::devices::getPackageDependencies()
{
    std::set<std::string, std::less<std::string>> deps;
    ADDDEPENDENCY(deps, toolbox);
    ADDDEPENDENCY(deps, xdaq);
    ADDDEPENDENCY(deps, gem::utils);
    ADDDEPENDENCY(deps, gem::hardware::utils);
    return deps;
}
