/// @file gem/hardware/devices/exception/Exception.h */

#ifndef GEM_HARDWARE_DEVICES_EXCEPTION_EXCEPTION_H
#define GEM_HARDWARE_DEVICES_EXCEPTION_EXCEPTION_H

#include "gem/utils/exception/Exception.h"

// The gem::hardware exceptions.
#define GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(EXCEPTION_NAME) GEM_DEFINE_EXCEPTION(EXCEPTION_NAME, hardware::devices)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(Exception)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(WriteValueMismatch)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ConfigurationParseProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ConfigurationProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ConfigurationValidationProblem)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(HardwareProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(UninitializedDevice)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(RCMSNotificationError)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(SOAPTransitionProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(NULLReadoutPointer)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(SoftwareProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(TransitionProblem)
GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ValueError)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(RPCMethodError)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(DeviceNameParseError)

GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ReadoutProblem)
// The gem::hardware alarms.
#define GEM_HARDWARE_DEVICES_DEFINE_ALARM(ALARM_NAME) GEM_HARDWARE_DEVICES_DEFINE_EXCEPTION(ALARM_NAME)

GEM_HARDWARE_DEVICES_DEFINE_ALARM(MonitoringFailureAlarm)

/// Macros to catch standard cases
/// @param base of the error message
/// @param exception type that is emitted

// FIXME: SHOULD I BE A MACRO OR A FUNCTION ELSEWHERE?
#define GEM_CATCH_RPC_ERROR(MSG_BASE, EX_TYPE)                         \
    catch (xhal::client::XHALRPCNotConnectedException const& e)        \
    {                                                                  \
        std::stringstream errmsg;                                      \
        errmsg << e.what();                                            \
        CMSGEMOS_ERROR(MSG_BASE << "error: " << errmsg.str());         \
        XCEPT_RAISE(EX_TYPE, errmsg.str());                            \
    }                                                                  \
    catch (xhal::client::XHALRPCException const& e)                    \
    {                                                                  \
        std::stringstream errmsg;                                      \
        errmsg << e.what();                                            \
        CMSGEMOS_ERROR(MSG_BASE << "error: " << errmsg.str());         \
        XCEPT_RAISE(EX_TYPE, errmsg.str());                            \
    }                                                                  \
    catch (gem::hardware::devices::exception::RPCMethodError const& e) \
    {                                                                  \
        std::stringstream errmsg;                                      \
        errmsg << e.what();                                            \
        CMSGEMOS_ERROR(MSG_BASE << "error: " << errmsg.str());         \
        XCEPT_RAISE(EX_TYPE, errmsg.str());                            \
    }

#endif // GEM_HARDWARE_DEVICES_EXCEPTION_EXCEPTION_H
