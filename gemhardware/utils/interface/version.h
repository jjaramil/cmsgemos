/// @file version.h

#ifndef GEM_HARDWARE_UTILS_VERSION_H
#define GEM_HARDWARE_UTILS_VERSION_H

#ifndef DOXYGEN_IGNORE_THIS

#include "config/PackageInfo.h"

namespace gem {
namespace hardware {
    namespace utils {

#define GEMHARDWAREUTILS_VERSION_MAJOR 1
#define GEMHARDWAREUTILS_VERSION_MINOR 0
#define GEMHARDWAREUTILS_VERSION_PATCH 1
#define GEMHARDWAREUTILS_PREVIOUS_VERSIONS "0.0.0,0.1.0,0.2.0,0.2.1,0.2.2,0.2.3,0.3.0,0.3.1,0.3.2,0.4.0,0.99.0,0.99.1,1.0.0"

#define GEMHARDWAREUTILS_VERSION_CODE PACKAGE_VERSION_CODE(GEMHARDWAREUTILS_VERSION_MAJOR, GEMHARDWAREUTILS_VERSION_MINOR, GEMHARDWAREUTILS_VERSION_PATCH)
#ifndef GEMHARDWAREUTILS_PREVIOUS_VERSIONS
#define GEMHARDWAREUTILS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(GEMHARDWAREUTILS_VERSION_MAJOR, GEMHARDWAREUTILS_VERSION_MINOR, GEMHARDWAREUTILS_VERSION_PATCH)
#else
#define GEMHARDWAREUTILS_FULL_VERSION_LIST GEMHARDWAREUTILS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(GEMHARDWAREUTILS_VERSION_MAJOR, GEMHARDWAREUTILS_VERSION_MINOR, GEMHARDWAREUTILS_VERSION_PATCH)
#endif

        const std::string project = "cmsgemos";
        const std::string package = "hardware::utils";
        const std::string versions = GEMHARDWAREUTILS_FULL_VERSION_LIST;
        const std::string summary = "Hardware interfaces for GEM devices";
        const std::string description = "";
        const std::string authors = "GEM Online Systems Group";
        const std::string link = "https://cms-gem-daq-project.github.io/cmsgemos/";

        config::PackageInfo getPackageInfo();
        void checkPackageDependencies();
        std::set<std::string, std::less<std::string>> getPackageDependencies();
    }
}
}

#endif // DOXYGEN_IGNORE_THIS

#endif // GEM_HARDWARE_VERSION_H
