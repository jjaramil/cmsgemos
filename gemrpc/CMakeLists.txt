cmake_minimum_required(VERSION 3.11 FATAL_ERROR)
# Since gemrpc is part of cmsgemos, there is no problem using main cmake folder
list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../cmake")

# This module is special in that it is built differently whether it targets
# the client or server machine.
# As a client, it only provides the RPC module interface.
# As a server, the complete RPC modules are built for the specific target.

get_property(GEMRPC_PARENT_DIRECTORY
    DIRECTORY
    PROPERTY PARENT_DIRECTORY
)

if(NOT "${GEMRPC_PARENT_DIRECTORY}" STREQUAL "")
    set(GEMRPC_CLIENT TRUE)
endif()

if(GEMRPC_CLIENT)
    cmsgemos_add_module(rpc)

    target_link_libraries(gemrpc INTERFACE xhal::common)

    # FIXME: The GEM variant should not be exported
    target_compile_definitions(gemrpc INTERFACE -DGEM_VARIANT=ge11)
else()
    # Prevent in-source builds
    include(PreventInSourceBuilds)

    project(gemrpc)

    # All files go inside the gempro user account directory
    if(CMAKE_INSTALL_PREFIX_INITIALIZED_TO_DEFAULT)
        set(CMAKE_INSTALL_PREFIX "/mnt/persistent/gempro/" CACHE PATH "Installation prefix" FORCE)
    endif()

    set(CMAKE_CXX_STANDARD 14)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)

    # FIXME: Make me an option
    add_definitions(-DGEM_VARIANT=ge11)

    include(GNUInstallDirs)
    include(ColorizeNinjaOutput)

    # Create the compile_commands.json file for external tools, e.g. linters
    set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

    #
    # Dependencies
    #
    # FIXME: A wrapper function would be welcome to build external packages
    add_subdirectory(../extern/xhal "${CMAKE_CURRENT_BINARY_DIR}/extern/xhal")
    add_subdirectory(../extern/reedmuller-c "${CMAKE_CURRENT_BINARY_DIR}/extern/reedmuller-c")

    #
    # Include the interface directory
    #
    file(MAKE_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/_interface/gem")
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        "${CMAKE_CURRENT_SOURCE_DIR}/interface"
        "${CMAKE_CURRENT_BINARY_DIR}/_interface/gem/rpc"
    )
    include_directories("${CMAKE_CURRENT_BINARY_DIR}/_interface")

    #
    # memhub is a regular library
    #
    add_library(memhub SHARED src/memhub.cpp)
    target_link_libraries(memhub PUBLIC memsvc)
    install(TARGETS memhub LIBRARY DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}/rpcmodules")

    #
    # RPC modules
    #
    function(gemrpc_add_module NAME)
        cmake_parse_arguments(ARGS "" "" "SOURCES;DEPS" ${ARGN})

        add_library(${NAME} SHARED ${ARGS_SOURCES})
        target_link_libraries(${NAME} PRIVATE ${ARGS_DEPS})
        target_link_libraries(${NAME} PRIVATE memhub)
        target_link_libraries(${NAME} PUBLIC xhal::common)
        target_link_libraries(${NAME} PRIVATE xhal::server)
        target_link_libraries(${NAME} PRIVATE log4cplus)

        # Remove the `lib` prefix
        set_target_properties(${NAME} PROPERTIES PREFIX "")

        install(TARGETS ${NAME} LIBRARY DESTINATION "${CMAKE_INSTALL_FULL_LIBDIR}/rpcmodules")
    endfunction()

    gemrpc_add_module(amc
        SOURCES
        src/amc.cpp src/amc/blaster_ram.cpp
        src/amc/daq.cpp src/amc/sca.cpp
        src/amc/ttc.cpp
        DEPS utils)
    gemrpc_add_module(calibration_routines
        SOURCES src/calibration_routines.cpp
        DEPS amc optohybrid utils vfat3)
    gemrpc_add_module(daq_monitor
        SOURCES src/daq_monitor.cpp
        DEPS amc utils)
    gemrpc_add_module(expert_tools
        SOURCES src/expert_tools.cpp
        DEPS utils)
    gemrpc_add_module(gbt
        SOURCES src/gbt.cpp
        DEPS utils)
    gemrpc_add_module(optohybrid
        SOURCES src/optohybrid.cpp
        DEPS amc utils)
    gemrpc_add_module(utils
        SOURCES src/utils.cpp src/XHALXMLParser.cpp
        DEPS lmdb xerces-c)
    target_include_directories(utils PUBLIC extern)
    gemrpc_add_module(vfat3
        SOURCES src/vfat3.cpp
        DEPS amc optohybrid reedmuller-c utils)
endif()
