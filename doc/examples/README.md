## Running local installation

After producing a local install of cmsgemos, make sure that you have also updated the `gemrpc` on the AMC card.
In order to do so, build `gemrpc` module and copy shared libraries to the AMC container.
Full sequnce starting from compilation, assuming that the build system is configured, is below. 
This is written for GEM 904 GE1/1 integration setup and should only be used as a reference!

```shell
cd /path/to/cmsgemos/
cmake3 --build _build --target install
cd gemrpc
DESTDIR=_install cmake3 --build _build --target install
scp -r _build/_install/mnt/persistent/gempro/lib/rpcmodules gempro@gem-shelf01-amc02:lib
# If changes were made to the rpcsvc source code
# scp _build/_install/mnt/persistent/gempro/bin/rpcsvc gempro@gem-shelf01-amc02:bin
# Please note that it requires a restart of rpcsvc under the gempro account
# LD_LIBRARY_PATH=/mnt/persistent/gempro/lib/rpcmodules:/mnt/persistent/gempro/lib GEM_PATH=/mnt/persistent/gemdaq bin/rpcsvc
cd -
sh doc/examples/scripts/run_xdaq.sh
```

This will launch the xdaq process with the default XML [configuration](xml/gemsupervisor.xml),
written for GEM 904 integration setup and using port 20100.

The GBT configurations examples can be found under `gemrpc/doc/gbt/<gem type>/`, but the files used for the configuration of the chamber are stored on the AMC folder `/mnt/persistent/gempro/etc/gem/gbt`.
The example files contain the phases derived as discussed in issue [#35](https://gitlab.cern.ch/cmsgemonline/cmsgemos/issues/35).
The files on the AMC contain the phases derived with a GBT phase scan.
