#!/bin/bash

SCRIPT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

export CMSGEMOS_LIBDIR="$(realpath "${SCRIPT_DIR}/../../../_build/_install/lib64/")"
export CMSGEMOS_DATADIR="$(realpath "${SCRIPT_DIR}/../../../_build/_install/share/cmsgemos/")"

# xDAQ related environment variables
export XDAQ_ROOT=/opt/xdaq
export XDAQ_DOCUMENT_ROOT="${CMSGEMOS_DATADIR}/htdocs"
export LD_LIBRARY_PATH=${XDAQ_ROOT}/lib:${LD_LIBRARY_PATH}

# GEM address tables
export GEM_ADDRESS_TABLE_PATH="${SCRIPT_DIR}/../xml"
export AMC13_ADDRESS_TABLE_PATH=/opt/cactus/etc/amc13

# Create the symlinks to xDAQ document root
(GLOBIGNORE=gemdaq; ln -snf /opt/xdaq/htdocs/* ${CMSGEMOS_DATADIR}/htdocs/)

# Start xDAQ
/opt/xdaq/bin/xdaq.exe -c ${SCRIPT_DIR}/../xml/gem904daq04.xml -p 20100 $@
