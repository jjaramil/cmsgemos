include(GNUInstallDirs)

# https://stackoverflow.com/a/27630120
function(prepend var prefix)
    set(listVar "")
    foreach(f ${ARGN})
        list(APPEND listVar "${prefix}/${f}")
    endforeach(f)
    set(${var} "${listVar}" PARENT_SCOPE)
endfunction(prepend)

function(cmsgemos_add_module name)
    cmake_parse_arguments(ARG "" "" "SOURCES" ${ARGN})

    # Append the module
    list(APPEND CMSGEMOS_MODULE ${name})

    # Build the required variables
    string(REPLACE ";" "_" LIBRARY_NAME "gem${CMSGEMOS_MODULE}")
    string(REPLACE ";" "/" INTERFACE_PATH "gem;${CMSGEMOS_MODULE}")
    string(REPLACE ";" "/" DATA_PATH "gem${CMSGEMOS_MODULE}")

    message(STATUS "Module ${LIBRARY_NAME} added: (interface: ${INTERFACE_PATH}; data: ${DATA_PATH})")

    # The public headers must be made available under gem/${name}.
    # This can be achieved with an appropriate symlink within the build
    # tree to the source tree. Although in-source builds are not
    # supported, they _should_ still work.
    get_filename_component(INTERFACE_DIR
        "${CMAKE_CURRENT_BINARY_DIR}/_interface/${INTERFACE_PATH}" DIRECTORY)
    file(MAKE_DIRECTORY "${INTERFACE_DIR}")
    # Could be replaced with file(CREATE_LINK ...) starting from CMake 3.14
    execute_process(
        COMMAND ${CMAKE_COMMAND} -E create_symlink
        "${CMAKE_CURRENT_SOURCE_DIR}/interface"
        "${CMAKE_CURRENT_BINARY_DIR}/_interface/${INTERFACE_PATH}"
    )

    # Create a shared library if the module has sources
    # Otherwise create an interface library
    if (ARG_SOURCES)
        prepend(SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/src" ${ARG_SOURCES})

        add_library(${LIBRARY_NAME} SHARED ${SOURCES})
        target_include_directories(${LIBRARY_NAME} PUBLIC "${CMAKE_CURRENT_BINARY_DIR}/_interface")

        install(TARGETS ${LIBRARY_NAME} LIBRARY DESTINATION ${CMAKE_INSTALL_FULL_LIBDIR})
    else()
        add_library(${LIBRARY_NAME} INTERFACE)
        target_include_directories(${LIBRARY_NAME} INTERFACE "${CMAKE_CURRENT_BINARY_DIR}/_interface")
    endif()


    # Install HTML files
    if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/html")
        install(DIRECTORY html
            DESTINATION
            "${CMAKE_INSTALL_FULL_DATAROOTDIR}/cmsgemos/htdocs/gemdaq/${DATA_PATH}")
    endif()

    # Install XML files
    if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/xml")
        install(DIRECTORY xml
            DESTINATION
            "${CMAKE_INSTALL_FULL_DATAROOTDIR}/cmsgemos/${DATA_PATH}")
    endif()

    # Export the module names to caller scope
    set(CMSGEMOS_MODULE "${CMSGEMOS_MODULE}" PARENT_SCOPE)
    set(CMSGEMOS_CURRENT_MODULE "${LIBRARY_NAME}" PARENT_SCOPE)
endfunction()
