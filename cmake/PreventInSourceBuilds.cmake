# Heavily inspired from https://stackoverflow.com/a/22636874

function(PreventInSourceBuilds)
  get_filename_component(SRCDIR "${CMAKE_SOURCE_DIR}" REALPATH)
  get_filename_component(BINDIR "${CMAKE_BINARY_DIR}" REALPATH)

  if("${SRCDIR}" STREQUAL "${BINDIR}")
    message("In-source build are disabled for this project.")
    message("Please run cmake from a subdirectory, e.g. _build.")
    message("Feel free to remove the CMakeCache.txt and CMakeFiles artifacts.")
    message(FATAL_ERROR "")
  endif()
endfunction()

PreventInSourceBuilds()
