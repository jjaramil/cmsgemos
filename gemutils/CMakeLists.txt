cmsgemos_add_module(
    utils
    SOURCES
    version.cpp
    GEMRegisterUtils.cpp
    Lock.cpp
    soap/GEMSOAPToolBox.cpp
)

target_link_libraries(gemutils PUBLIC xDAQ::config)
target_link_libraries(gemutils PUBLIC xDAQ::log4cplus)
target_link_libraries(gemutils PUBLIC xDAQ::toolbox)
target_link_libraries(gemutils PUBLIC xDAQ::xcept)
target_link_libraries(gemutils PUBLIC xDAQ::xdaq)
target_link_libraries(gemutils PUBLIC xDAQ::xdata)
target_link_libraries(gemutils PUBLIC xDAQ::xerces-c)
target_link_libraries(gemutils PUBLIC xDAQ::xoap)
